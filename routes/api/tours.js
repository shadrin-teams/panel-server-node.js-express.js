let express = require('express');
let router = express.Router();
const moduleKey = 'tours';

router.get('/' + moduleKey, function(req, res, next) {
  const db = req.app.get('db');
  db.Tours.findAll()
    .then((data) => {
      res.send({status: 200, data});
    })
    .catch(error => {
      res.send({status: 500, error});
    })

});

module.exports = router;