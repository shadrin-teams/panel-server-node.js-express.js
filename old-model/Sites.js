let mongoose = require('mongoose');

let schema = new mongoose.Schema({
    id: {type: String, required: true, unique: true},
    name: {type: String, required: true},
});

let Sites = mongoose.model('sites', schema);

module.exports = Sites;