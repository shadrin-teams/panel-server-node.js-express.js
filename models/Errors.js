let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let Errors = sequelize.define('Errors', {
    error: {
      type: Sequelize.STRING,
      allowNull: false
    },
    errorType: {
      type: Sequelize.STRING,
      allowNull: false
    },
    url: {
      type: Sequelize.STRING,
    },
    data: {
      type: Sequelize.STRING,
    },
    creatAt: {
      type: Sequelize.DATE,
    },
    updateAt: {
      type: Sequelize.DATE,
    },
  });

  return Errors;
};