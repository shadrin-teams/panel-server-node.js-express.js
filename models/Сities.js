let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Сities', {
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      country_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      }
    }
  );
};