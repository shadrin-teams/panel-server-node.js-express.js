let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Users_rights', {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    right_id: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    create: {
      type: Sequelize.BOOLEAN,
    },
    read: {
      type: Sequelize.BOOLEAN,
    },
    update_action: {
      type: Sequelize.BOOLEAN,
    },
    delete_action: {
      type: Sequelize.BOOLEAN,
    },
    createdAt: {
      type: Sequelize.DATE,
    },
    updatedAt: {
      type: Sequelize.DATE,
    }
  }, {
    freezeTableName: true // Model tableName will be the same as the model name
  });

};