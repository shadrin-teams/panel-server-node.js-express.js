let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  queue = require('queue'),
  moduleKey = 'sub-section';

/**
 * Вызвращает список продукции
 */

router.get('/' + moduleKey + '/:ownerId', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let ownerId = req.params.ownerId;
  let model = require('../../models/Sections');
  let Keywords = require('../../models/Keywords');
  let list = [];
  model.find({owner: ownerId}, (error, data) => {

    if (error) res.send({status: 'error', error});
    else {
      for (let i = 0; i < data.length; i++) {
        let obj = {};
        Object.keys(Keywords.schema.obj).forEach((field) => {
          obj[field] = data[i][field];
        });
        obj.count = 0;
        list.push(obj);
      }

      let q = queue({concurrency: 1});

      // Собираем количество кулючевых слова у каждой категории
      for (let i = 0; i < list.length; i++) {
        q.push((next) => {
          Keywords.count({section: list[i].id}, (errorSub, countSub) => {
            if (!errorSub) {
              list[i].count = countSub;
            }
            next(errorSub);
          });
        })
      }

      q.start((err) => {
        if (err) res.send({status: 'error', error: err});
        else {
          res.send({status: 'ok', list});
        }
      })
    }
  })
});

router.get('/' + moduleKey + '/:sectionId', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let sectionId = req.params.sectionId;
  let model = require('../../models/Sections');
  console.log('id', sid);
  model.findOne({owner: sectionId}, function(err, data) {
    if (err) res.send({error: err});
    else res.send({data, status: 'ok'});
  });
});

/**
 * Удаление
 */

router.delete('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id;
  if (id) {
    let model = require('../../models/Sections');
    model.remove({id: id}, function(err) {
      if (!err) res.send({status: 'deleted'});
      else res.send({status: false, error: err});
    })
  }
  else res.send({data: '', error: 'no id'});

});

router.post(
  '/' + moduleKey + '/:siteId' + '/:sectionId', passport.authenticate('jwt', {session: false}),
  function(req, res, next) {

    let siteId = req.params.siteId;
    let sectionId = req.params.sectionId;
    let data = req.body.data;

    if (data) {
      let model = require('../../models/Sections');
      let Model = new model();
      Model.id = Model._id;

      Object.keys(data).forEach((field) => {
        Model[field] = data[field];
      });

      if (!Model['site']) {
        Model['site'] = siteId
      }

      if (!Model['owner']) {
        Model['owner'] = sectionId
      }

      console.log('add Model', Model);
      Model.save(function(err, data) {

        if (err) {
          console.log('add err', err, Model);
          res.send({error: err});
        }
        else {
          console.log('add ok', Model);
          res.send({data: data, status: true});
        }

      });
    } else {
      res.send({data: '', error: 'No data'});
    }

  }
);

router.put('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id,
    data = req.body.data;

  if (data && data.id) {

    let model = require('../../models/Sections');
    model.findById(data.id, (error, Model) => {
      if (data) {
        Object.keys(data).forEach((field) => {
          Model[field] = data[field];
        });
        Model.save(function(err, data) {
          if (err) {
            res.send({error, status: 'fail'});
          } else {
            res.send({data: {item: data}, status: 'ok'});
          }
        });
      } else {
        res.send({error: 'incorrect id', status: 'fail'});
      }
    });
  } else {
    res.send({'error': 'incorrect data', status: 'fail'});
  }

});

module.exports = router;