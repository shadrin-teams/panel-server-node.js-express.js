let express = require('express');
let router = express.Router();
let path = require('path');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let expressSession = require('express-session');
let bodyParser = require('body-parser');
let methodOverride = require('method-override');

// Роутинг
let routesIndex = require('./routes'),
  apiRoutes = require('./routes/api'),
  db = require('./libs/db');

let Users = require('./models/Users'),
  config = require('./config/main');

// Авторизация
let passport = require('passport'),
  JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;

let app = express();

// view engine setup
app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// DB
app.set('db', db);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressSession({secret: 'keyboard cat', resave: true, saveUninitialized: true}));
app.use(cookieParser());
app.use(methodOverride('_method'));

let opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
opts.secretOrKey = config.secret;
passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
  db.Users.find({where: {id: jwt_payload.id}})
    .then(user => {
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    })
    .catch(error => {
      return done(error, false);
    });
}));

app.use(express.static(path.join(__dirname, 'public'), {maxAge: 86400000}));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Authorization,content-type, JWT, transformRequest');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use('/', routesIndex);
app.use('/api', apiRoutes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  //console.log('Error', err);
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: err.message
  });
});

module.exports = app;