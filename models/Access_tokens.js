let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Access_tokens', {
    user_id: {
      type: Sequelize.INTEGER,
    },
    ip: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    token: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    exp_date: {
      type: Sequelize.INTEGER,
    },
    createdAt: {
      type: Sequelize.DATE,
    },
    updatedAt: {
      type: Sequelize.DATE,
    }
  }, {
    freezeTableName: true // Model tableName will be the same as the model name
  });

};