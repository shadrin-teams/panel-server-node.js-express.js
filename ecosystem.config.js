module.exports = {
  apps: [{
    'name': 'panel_travel',
    'script': './bin/www',
    'watch': true,
    "ignore_watch" : ["public/files"],
    /*    'out_file': 'combined.log',
        'error_file': 'combined.log',*/
    'env': {
      'NODE_ENV': 'development'
    },
    'env_production': {
      'NODE_ENV': 'production'
    },
  }]
};