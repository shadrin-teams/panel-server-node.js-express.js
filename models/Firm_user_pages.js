let Sequelize = require('sequelize');
let helper = require('../libs/helper');

module.exports = (sequelize, DataTypes) => {
  let Firm_user_pages = sequelize.define('Firm_user_pages', {
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      firm_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      order: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      slug: {
        type: Sequelize.STRING,
        allowNull: false
      },
      is_valid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    }, {
      associate: (models) => {
        Firm_user_pages.belongsTo(
          models.Firms, {foreignKey: 'FUP_firm_id', targetKey: 'id', onDelete: 'CASCADE', hooks: true});
      }
    }
  );

  Firm_user_pages.beforeBulkDestroy((instance) => {
    console.log('--------------------Firm_user_pages.beforeBulkDestroy');
    return helper.deleteRelatedFiles(sequelize.models, sequelize.models.Firm_user_pages, 'firms-page', instance.where);
  });

  return Firm_user_pages;
};