let express = require('express'),
  router = express.Router(),
  queue = require('queue'),
  _ = require('underscore'),
  moment = require('moment'),
  passport = require('passport'),
  validator = require('validator'),
  sequelize = require('sequelize');

let helper = require('../../libs/helper'),
  publishHelper = require('../../libs/publish_rules/publish.helper'),
  constants = require('../../constants'),
  embedHelper = require('../../libs/embed.helper');
const moduleKey = 'firms';

router.get(`/${moduleKey}`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const userId = +req.user.id;
  const embed = req.query.embed || '';
  const q = queue({concurrency: 1});
  let countAll = 0;
  let items = [];
  const limit = +req.query.limit || 10;
  const offset = +req.query.offset || 0;
  let data = {};
  const uploadFileType = 'firms';
  let excludeField = [];
  if (embed.indexOf('recomm') === -1) {
    excludeField.push('errors');
    excludeField.push('recomm');
  }

  q.push(next => {
    db.Firms.findAndCountAll({
      where: {user_id: userId},
      limit,
      offset,
      raw: true,
      attributes: {exclude: excludeField},
      order: [['order'], ['id']]
    })
      .then((result) => {
        data.items = result.rows;
        data.count = result.count;
        next();
      })
      .catch(error => {
        next(error);
      })
  });

  if (!embed || embed.indexOf('recomm') === -1) {
    delete data.errors;
    delete data.recomm;
  }

  if (embed) {
    if (embed.indexOf('images') > -1) {
      q.push((next) => {
        db.Files.findAll({
          where: {
            section: uploadFileType,
            user_id: userId
          },
          attributes: ['id', 'file', 'folder', 'main', 'order', 'item_id'],
          raw: true
        })
          .then((images) => {
            const groupImage = _.groupBy(images, item => item.item_id);
            data.items.forEach(item => {
              if (groupImage[item.id]) {
                item.images = groupImage[item.id];
              }
            });
            next();
          })
          .catch((error) => {
            next(error);
          });
      });
    }

    if (embed.indexOf('statistic') > -1) {
      q.push(next => {
        db.Firm_user_services.findAll({
          attributes: ['firm_id', sequelize.fn('count', sequelize.col('id'))],
          where: {user_id: userId},
          group: ['firm_id'],
          raw: true
        })
          .then(list => {
            const result = {};
            list.forEach(item => {
              if (item.count > 0) {
                result[item.firm_id] = item.count;
              }
            });
            data.items.forEach(item => {
              if (result[item.id]) {
                item.statistic = {services: +result[item.id]};
              }
            });
            next();
          })
          .catch(error => {
            next(error);
          })
      });

      q.push(next => {
        db.Firm_user_pages.findAll({
          attributes: ['firm_id', sequelize.fn('count', sequelize.col('id'))],
          where: {user_id: userId},
          group: ['firm_id'],
          raw: true
        })
          .then(list => {
            const result = {};
            list.forEach(item => {
              if (item.count > 0) {
                result[item.firm_id] = item.count;
              }
            });
            data.items.forEach(item => {
              if (result[item.id]) {
                if (!item.statistic) item.statistic = {};
                item.statistic.pages = +result[item.id];
              }
            });
            next();
          })
          .catch(error => {
            next(error);
          })
      });
    }
  }

  q.start(error => {
    if (!error) {
      res.send({status: 200, data});
    } else {
      res.status(500).send({status: 500, error});
    }
  });

});

router.get(`/${moduleKey}/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = +req.params.id;
  const userId = +req.user.id;
  const embed = req.query.embed ? req.query.embed.split(',').map(item => item.trim()) : null;
  let data = {};
  let error = '';
  const q = queue({concurrency: 1});

  let excludeField = [];
  if (embed && embed.indexOf('recomm') === -1) {
    excludeField.push('errors');
    excludeField.push('recomm');
  }

  q.push(next => {
    db.Firms.findById(id, {
      raw: true
    })
      .then((firm) => {
        data = firm;
        next();
      })
      .catch(err => {
        error = err;
        next(err);
      })
  });

  if (!embed || embed.indexOf('recomm') === -1) {
    delete data.errors;
    delete data.recomm;
  }

  if (embed) {
    if (embed.indexOf('moderation') > -1) {
      q.push((next) => {
        if (id) {
          db.Firm_moderations.findOne({
            where: {
              firm_id: id
            },
            raw: true,
            attributes: ['id', 'date', 'date_limit']
          })
            .then((item) => {
              if (item.id) {
                data.moderation = {
                  ...item
                };
              }
              next();
            })
            .catch((error) => {
              next(error);
            });

        } else {
          next();
        }
      });
    }

    if (embed.indexOf('images') > -1) {
      q.push((next) => {
        if (id) {
          db.Files.findAll({
            where: {
              section: 'firms',
              item_id: id,
              user_id: userId
            },
            attributes: ['id', 'file', 'folder', 'main', 'order'],
            order: [['order', 'asc']]
          })
            .then((images) => {
              if (images) {
                data.images = images.map(image => ({
                  id: image.dataValues.id,
                  folder: image.dataValues.folder,
                  file: image.dataValues.file,
                  order: image.dataValues.order,
                  main: image.dataValues.main
                }));
              }
              next();
            })
            .catch((error) => {
              next();
            });

        } else {
          next();
        }
      });
    }

    if (embed.indexOf('services') > -1) {
      q.push(next => {
        db.Firm_user_services.findAll({
          where: {
            firm_id: id,
            user_id: req.user.id
          },
          order: [['order']]
        })
          .then((services) => {
            data.services = services.map(item => item.dataValues) || [];
            next();
          })
          .catch(error => {
            next(error);
          })
      });

      // Подгружаем картинки
      q.push((next) => {
        if (data.services && data.services.length) {
          db.Files.findAll({
            where: {
              section: 'firms-service',
              user_id: userId,
              item_id: {$in: data.services.map(item => item.id)}
            },
            attributes: ['id', 'file', 'folder', 'main', 'order', 'item_id']
          })
            .then((images) => {
              const listImages = images.map(image => ({
                  id: image.dataValues.id,
                  folder: image.dataValues.folder,
                  file: image.dataValues.file,
                  main: image.dataValues.main,
                  item_id: image.dataValues.item_id,
                  order: image.dataValues.order
                })
              );
              const groupImage = _.groupBy(listImages, item => item.item_id);
              data.services.forEach(item => {
                if (groupImage[item.id]) {
                  item.images = groupImage[item.id];
                }
              });
              next();
            })
            .catch((error) => {
              next(error);
            });
        } else {
          next();
        }
      });
    }

    if (embed.indexOf('pages') > -1) {
      q.push(next => {
        db.Firm_user_pages.findAll({
          where: {
            firm_id: id,
            user_id: req.user.id
          },
          order: [['order']]
        })
          .then((pages) => {
            data.pages = pages.map(item => item.dataValues) || {};
            next();
          })
          .catch(error => {
            next(error);
          })
      });

      // Подгружаем картинки
      q.push((next) => {
        if (data.pages && data.pages.length) {
          db.Files.findAll({
            where: {
              section: 'firms-page',
              user_id: userId,
              item_id: {$in: data.pages.map(item => item.id)}
            },
            attributes: ['id', 'file', 'folder', 'main', 'order', 'item_id']
          })
            .then((images) => {
              const listImages = images.map(image => ({
                  id: image.dataValues.id,
                  folder: image.dataValues.folder,
                  file: image.dataValues.file,
                  main: image.dataValues.main,
                  item_id: image.dataValues.item_id,
                  order: image.dataValues.order
                })
              );
              const groupImage = _.groupBy(listImages, item => item.item_id);
              data.pages.forEach(item => {
                if (groupImage[item.id]) {
                  item.images = groupImage[item.id];
                }
              });
              next();
            })
            .catch((error) => {
              next(error);
            });
        } else {
          next();
        }
      });
    }

    if (embed.indexOf('country') > -1) {
      q.push(next => {
        embedHelper.country(db, data.country_id)
          .then((country) => {
            data.country = country ? {id: country.id, name: country.name} : {};
            next();
          })
          .catch(error => {
            next(error);
          });
      });
    }

    if (embed.indexOf('city') > -1) {
      q.push(next => {
        embedHelper.city(db, data.city_id)
          .then((city) => {
            data.city = city ? {id: city.id, name: city.name, country_id: city.country_id} : {};
            next();
          })
          .catch(error => {
            next(error);
          });
      });
    }
  }

  q.start(() => {
    if (error) res.status(200).send({status: 500, error});
    else res.send({status: 200, data});
  });

});

router.get(`/${moduleKey}/:id/validate`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = +req.params.id;
  const userId = req.user.id;
  let data;

  publishValidate(db, id, true)
    .then((data) => {
      if (data) {
        res.status(200).send({
          ...data,
          status: 500
        });
      } else {
        res.send({...data, status: 200});
      }
    });
});

router.get(
  `/${moduleKey}/:id/publication/:status(2|3)`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const db = req.app.get('db');
    const id = +req.params.id;
    const status = +req.params.status;
    const userId = req.user.id;
    let firmData = {};
    let data = {};
    const q = queue({concurrency: 1});

    if (status === 3 && userId !== constants.adminId) {
      res.status(403).send({status: 403});
      return;
    }

    q.push(next => {
      publishValidate(db, id, true)
        .then((validateData) => {
          data = validateData;
          next();
        });
    });

    q.push(next => {
      if (!data.errors || !data.errors.length) {
        let where = {id, user_id: userId};
        // if (status === 3) where['status'] = 2;

        db.Firms.update(
          {status: status},
          {
            where,
            returning: true,
            raw: true
          }
        )
          .then(data => {
            if (data[0] === 1 && data[1][0].user_id) {
              firmData = data[1][0];
              next();
            } else {
              next('FIRM_ID_OR_STATUS_INCORRECT');
            }
          })
          .catch(error => {
            next(error);
          });
      } else {
        next();
      }
    });

    q.push(next => {
      if (!data.errors || !data.errors.length) {
        const itemData = {
          firm_id: id
        };

        if (status === 2) {
          itemData['date'] = moment().format('YYYY-MM-DD');
          itemData['date_limit'] = moment().add(3, 'days').format('YYYY-MM-DD');
        }

        if (status === 3) {
          itemData['archive'] = true;
          itemData['publish_user'] = userId;
          itemData['publish_date'] = moment();
        }

        helper.updateOrCreate(db.Firm_moderations, {firm_id: id}, itemData)
          .then(moderation => {
            data.moderations = itemData;
            next();
          })
          .catch(error => {
            next(error);
          });
      } else {
        next();
      }
    });

    if (status === 3) {
      q.push(next => {
        // отправить сообщение пользователю
        db.Messages.create({
          date: moment().format('YYYY-MM-DD'),
          type: 'validation',
          user_to: firmData.user_id,
          user_from: constants.adminId,
          subject: `Фирма №${firmData.id} опубликованна`,
          message: `Фирма №${firmData.id} опубликованна`,
          substance: 'firm',
          substance_id: firmData.id
        })
          .then(data => {
            next();
          })
          .catch(error => {
            console.log('error', error);
            next(error);
          });
      });
    }

    q.start((error) => {
      if (error || (data.errors && data.errors.length)) {
        res.send({
          ...data,
          status: 500,
          error
        });
      } else {
        res.send({...data, status: 200, error});
      }
    });
  });

router.get(`/${moduleKey}/:id/no-publish`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = +req.params.id;
  const user_id = req.user.id;
  let data;
  const q = queue({concurrency: 1});

  q.push(next => {
    db.Firms.update(
      {status: 1},
      {where: {id, user_id}}
    )
      .then(data => {
        next();
      })
      .catch(error => {
        next(error);
      });
  });

  q.push(next => {
    db.Firm_moderations.destroy({where: {firm_id: id}})
      .then(() => {
        next();
      })
      .catch(error => {
        next(error);
      });
  });

  q.start((errors) => {
    res.status(200).send({status: errors ? 500 : 200});
  });
});

router.get(`/${moduleKey}/form/:id?`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = req.params.id;
  let data = {};
  let error = '';
  let q = queue({concurrency: 1});

  if (id) {
    q.push(next => {
      db.Firms.findById(id)
        .then((firm) => {
          data = {firm};
          next();
        })
        .catch(err => {
          error = err;
          next();
        })
    })
  }

  q.push(next => {
    db.Countries.findAll({attributes: ['id', 'name']})
      .then(items => {
        data.countries = {items, count: items.length};
        next();
      })
      .catch(err => {
        error = err;
        next();
      })
  });

  q.push(next => {
    db.Сities.findAll({attributes: ['id', 'name', 'country_id']})
      .then((items) => {
        data.cities = {items, count: items.length};
        next();
      })
      .catch(err => {
        error = err;
        next();
      })
  });

  q.start(() => {
    if (error) res.status(500).send({status: 500, error});
    else res.send({status: 200, data});
  });

});

router.put(
  `/${moduleKey}/:id/:section(common|about)`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const data = req.body;
    const db = req.app.get('db');
    const id = req.params.id;
    let q = queue({concurrency: 1});
    let responseData = {};
    let errorData = {};

    q.push(next => {
      if (firmSectionValidate(req.params.section)) {
        db.Firms.update(data, {where: {id, status: {in: [1, 2]}}})
          .then(response => {
            return db.Firms.findById(id)
          })
          .then(response2 => {
            responseData = response2;
            next();
          })
          .catch(error => {
            errorData = helper.normaliseSequelizeErrors(error.errors);
            next();
          });

      } else {
        errorData = 'VALIDATE_ERROR';
        next();
      }
    });

    q.push(next => {
      publishValidate(db, id, true)
        .then(result => {
          if (result.recomm) responseData.recomm = result.recomm;
          if (result.errors) responseData.errors = result.errors;
          next();
        });
    });

    q.start(error => {
      if (errorData) {
        res.send({status: 200, data: responseData});
      } else {
        res.send({status: 400, error: errorData});
      }
    });
  }
);

router.post(`/${moduleKey}`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const data = req.body;
  data.user_id = req.user.id;

  const errors = firmSectionValidate(data, 'common');
  if (!errors.length) {
    db.Firms.create(data)
      .then(data => {
        res.send({status: 200, data});
      })
      .catch(error => {
        res.send({status: 400, error: {list: helper.normaliseSequelizeErrors(error.errors)}});
      });
  } else {
    res.send({status: 400, error: errors});
  }
});

router.post(`/${moduleKey}/sort`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const data = req.body;
  const userId = +req.user.id;
  const q = queue();

  data.forEach(file => {
    q.push(next => {
      db.Firms.update(
        {order: file.order},
        {
          where: {
            user_id: userId,
            id: file.id,
            status: {in: [1, 2]}
          }
        }
      )
        .then(data => {
          next();
        })
        .catch(error => {
          next(error);
        });
    });
  });

  q.start(error => {
    if (!error) {
      res.send({status: 200});
    } else {
      res.send({status: 500, error});
    }
  });

});

router.delete(`/${moduleKey}/:id/image`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');

  db.Firms.findById(req.params.id, {attributes: ['image']})
    .then((data) => {
      res.send({status: 200, data});
    })
    .catch(error => {
      res.status(500).send({status: 500, error});
    });
});

router.delete(`/${moduleKey}/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = req.params.id;

  db.Firms.destroy({
    where: {
      id: id,
      user_id: req.user.id
    }
  })
    .then(() => {
      // @TODO Нужно сделать каскадное удаление связанных услуг и страниц вместе с их файлами
      res.send({status: 200});
    })
    .catch(error => {
      res.send({status: 400, error});
    });
});

firmSectionValidate = (data, section) => {
  let rules = [];
  let errors = [];
  switch (section) {
    case 'common' :
      rules = [
        {field: 'name', rules: {require: true}},
        {field: 'email', rules: {require: true, email: true}},
        {field: 'country_id', rules: {require: true, number: true}},
        {field: 'city_id', rules: {require: true, number: true}},
        {field: 'phone', rules: {require: true}}
      ];
      break;
  }

  rules.forEach((rule) => {
    Object.keys(rule.rules).forEach((ruleName) => {
      const error = checkRule(data, rule.field, ruleName);
      if (error) errors.push(error);
    });

  });

  return errors;
};

checkRule = (data, field, rule) => {
  let error = '';
  switch (rule) {
    case 'require' :
      if (!data[field]) error = 'require';
      break;
    case 'number' :
      if (data[field] && +data[field] <= 0) error = 'number_format_error';
      break;
    case 'email' :
      if (data[field] && !validator.isEmail(data[field])) error = 'email_format_error';
      break;
  }
  return error;
};

publishValidate = (db, firmId, needSave = false) => {
  let data = {};
  return new Promise((solved, reject) => {
    publishHelper.validate(db, 'Firms', firmId, needSave)
      .then(dataRes => {
        if (dataRes.recommendations) data.recomm = JSON.stringify(dataRes.recommendations);
        if (dataRes.errors) data.errors = JSON.stringify(dataRes.errors);
        solved(data);
      })
      .catch(dataList => {
        if (dataList.recommendations) data.recomm = JSON.stringify(dataList.recommendations);
        if (dataList.errors) data.errors = JSON.stringify(dataList.errors);
        solved(data);
      });
  });
};

module.exports = router;