let mongoose = require('mongoose');

let schema = new mongoose.Schema({
    id: {type: String, required: true, unique: true},
    name: {type: String, required: true},
    site: {type: String, required: true},
    owner: {type: String},
});

let Sections = mongoose.model('sections', schema);

module.exports = Sections;