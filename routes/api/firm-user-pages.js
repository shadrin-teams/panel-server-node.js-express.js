let express = require('express'),
  router = express.Router(),
  helper = require('../../libs/helper'),
  passport = require('passport'),
  queue = require('queue'),
  _ = require('underscore'),
  publishHelper = require('../../libs/publish_rules/publish.helper'),
  config = require('../../config/main');

const moduleKey = 'firm-user-pages';
const uploadFileType = 'firms-page';

router.get(`/${moduleKey}/:firmId`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const embed = req.query.embed ? req.query.embed.split(',').map(item => item.trim()) : null;
  const userId = req.user.id;

  const q = queue({concurrency: 1});
  let data = {};

  q.push(next => {
    db.Firm_user_pages.findAndCountAll({
      where: {
        user_id: req.user.id,
        firm_id: req.params.firmId
      },
      order: [['order', 'asc'], ['id', 'desc']],
      raw: true
    })
      .then((result) => {
        data.items = result.rows;
        data.count = result.count;
        next();
      })
      .catch(error => {
        next(error);
      })
  });

  if (embed) {
    if (embed.indexOf('images') > -1) {
      q.push((next) => {

        db.Files.findAll({
          where: {
            section: uploadFileType,
            user_id: userId
          },
          attributes: ['id', 'file', 'folder', 'main', 'order', 'item_id']
        })
          .then((images) => {
            const listImages = images.map(image => ({
                id: image.dataValues.id,
                folder: image.dataValues.folder,
                file: image.dataValues.file,
                main: image.dataValues.main,
                item_id: image.dataValues.item_id,
                order: image.dataValues.order
              })
            );
            const groupImage = _.groupBy(listImages, item => item.item_id);
            data.items.forEach(item => {
              if (groupImage[item.id]) {
                item.images = groupImage[item.id];
              }
            });
            next();
          })
          .catch((error) => {
            next(error);
          });
      });
    }
  }

  q.start(error => {
    if (!error) res.send({status: 200, data});
    else res.send({status: 400, error});
  });
});

router.get(`/${moduleKey}/:firmId/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const q = queue({concurrency: 1});
  const id = +req.params.id;
  const embed = req.query.embed ? req.query.embed.split(',').map(item => item.trim()) : null;
  let data = [];

  q.push(next => {
    db.Firm_user_pages.findOne({
      where: {
        id: req.params.id,
        user_id: req.user.id,
        firm_id: req.params.firmId
      }
    })
      .then((service) => {
        data = service.dataValues;
        next();
      })
      .catch(error => {
        next(error);
      })
  });

  if (embed) {
    if (embed.indexOf('images') > -1) {
      q.push((next) => {
        db.Files.findAll({
          where: {
            section: uploadFileType,
            item_id: id
          },
          attributes: ['id', 'file', 'folder', 'main', 'order'],
          order: [
            ['order', 'asc'],
            ['id', 'desc']
          ]
        })
          .then((images) => {
            data.images = images.map(image => ({
                id: image.dataValues.id,
                folder: image.dataValues.folder,
                file: image.dataValues.file,
                main: image.dataValues.main
              })
            );
            next();
          })
          .catch((error) => {
            next();
          });
      });
    }
  }

  q.start(error => {
    if (!error) {
      res.send({status: 200, data});
    } else {
      res.send({status: 400, error});
    }
  });
});

router.put(`/${moduleKey}/:firmId/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const data = req.body;
  const q = queue({concurrency: 1});
  let isValid = false;

  if (!data.firm_id || !data.id) {
    res.send({status: 400, error: 'REQUIRED_FIELDS_ARE_NOT_FILLED'});
  }

  q.push(next => {
    publishHelper.validate(db, 'Firm_user_pages', data.id)
      .then(data => {
        isValid = true;
        next();
      })
      .catch(data => {
        isValid = false;
        next();
      });
  });

  q.start(error => {
    data.is_valid = isValid;
    db.Firm_user_pages.update(
      data,
      {
        where: {
          id: req.params.id,
          firm_id: req.params.firmId,
          user_id: req.user.id
        }
      }
    )
      .then(data => {
        res.send({status: 200, data});
      })
      .catch(error => {
        res.send({status: 400, error: helper.normaliseSequelizeErrors(error.errors)});
      });
  });

});

router.put(`/${moduleKey}/:firmId/sort`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const listData = req.body.list;
  const q = queue({concurrency: 1});
  const listUpdated = [];

  if (!listData.length) {
    res.send({status: 400, error: 'NO HAVE LIST DATA'});
  }

  listData.forEach((item) => {
    q.push((next) => {
      db.Firm_user_pages.update(
        {order: item.pos},
        {
          where: {
            id: item.id,
            firm_id: req.params.firmId,
            user_id: req.user.id
          }
        }
      )
        .then((items) => {
          next();
        })
        .catch((error) => {
          next(error);
        });
    });

    q.start((err) => {
      if (err) {
        res.send({status: 400, error});
      } else {
        res.send({status: 200, listUpdated});
      }
    });
  })

});

router.delete(`/${moduleKey}/:firmId/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = req.params.id;

  db.Firm_user_pages.destroy({
    where: {
      id: id,
      firm_id: req.params.firmId,
      user_id: req.user.id
    }
  })
    .then(() => {
      helper.deleteRelatedFiles(db, uploadFileType, id);
      res.send({status: 200});
    })
    .catch(error => {
      res.send({status: 400, error});
    });
});

router.post(`/${moduleKey}/sort`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const data = req.body;
  const userId = +req.user.id;
  const q = queue();

  data.forEach(file => {
    q.push(next => {
      db.Firm_user_pages.update(
        {order: file.order},
        {where: {user_id: userId, id: file.id}}
      )
        .then(data => {
          next();
        })
        .catch(error => {
          next(error);
        });
    });
  });

  q.start(error => {
    if (!error) {
      res.send({status: 200});
    } else {
      res.send({status: 500, error});
    }
  });

});

router.post(`/${moduleKey}/:firmId`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const data = req.body;
  const db = req.app.get('db');
  const user_id = req.user.id;
  const q = queue({concurrency: 1});
  let isValid = false;

  if (!data.firm_id) data.firm_id = +req.params.firmId;
  if (!data.firm_id) {
    res.send({status: 400, error: 'REQUIRED_FIELDS_ARE_NOT_FILLED'});
  }

  data.user_id = user_id;

  q.push(next => {
    publishHelper.validate(db, 'Firm_user_pages')
      .then(data => {
        isValid = true;
        next();
      })
      .catch(data => {
        isValid = false;
        next();
      });
  });

  q.start(error => {
    db.Firms.findById(+data.firm_id)
      .then((firm) => {
        firm.user_id = req.user.id;
        data.is_valid = isValid;
        return db.Firm_user_pages.create(data)
      })
      .then(data => {
        res.send({status: 200, data});
      })
      .catch(error => {
        res.send({status: 400, error: helper.normaliseSequelizeErrors(error.errors)});
      });

  });
});

module.exports = router;