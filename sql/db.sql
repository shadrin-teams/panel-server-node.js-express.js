-- Table: public."Access_tokens"

-- DROP TABLE public."Access_tokens";

CREATE TABLE public."Access_tokens"
(
  id integer DEFAULT nextval('access_token_id_seq'::regclass),
  user_id integer,
  ip character varying(15) NOT NULL,
  token character varying(255) NOT NULL,
  exp_date integer,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Access_tokens"
  OWNER TO postgres;

CREATE TABLE public."Countries"
(
  id integer DEFAULT nextval('countries_id_seq'::regclass),
  name character varying(150) NOT NULL,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Countries"
  OWNER TO postgres;

CREATE TABLE public."Files"
(
  id integer DEFAULT nextval('files_id_seq'::regclass),
  file character varying(150) NOT NULL,
  folder character varying(150) NOT NULL,
  is_used boolean default false,
  section integer not null,
  item_id integer,
  user_id integer not null,
  type character varying(50) NOT NULL,
  main boolean default false,
  "order" integer default 100,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Files"
  OWNER TO postgres;

CREATE TABLE public."Firm_moderations"
(
  id integer DEFAULT nextval('firm_moderations_id_seq'::regclass),
  firm_id integer not null,
  "date" date,
  "date_limit" date,
  archive boolean default false,
  publish_user integer not null,
  "publish_date" date,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Firm_moderations"
  OWNER TO postgres;

CREATE TABLE public."Firm_pages"
(
  id integer DEFAULT nextval('firm_pages_id_seq'::regclass),
  name character varying(150) NOT NULL,
  slug character varying(150) NOT NULL,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Firm_pages"
  OWNER TO postgres;

CREATE TABLE public."Firm_services"
(
  id integer DEFAULT nextval('firm_services_id_seq'::regclass),
  name character varying(150) NOT NULL,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Firm_services"
  OWNER TO postgres;

CREATE TABLE public."Firm_user_pages"
(
  id integer DEFAULT nextval('firm_user_pages_id_seq'::regclass),
  name character varying(150) NOT NULL,
  description text NOT NULL,
  user_id integer NOT NULL,
  firm_id integer NOT NULL,
  "order" integer default 100,
  slug character varying(150) NOT NULL,
  is_valid boolean default false,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Firm_user_pages"
  OWNER TO postgres;

CREATE TABLE public."Firm_user_services"
(
  id integer DEFAULT nextval('firm_user_services_id_seq'::regclass),
  name character varying(150) NOT NULL,
  description text NOT NULL,
  user_id integer NOT NULL,
  firm_id integer NOT NULL,
  "order" integer default 100,
  slug character varying(150) NOT NULL,
  is_valid boolean default false,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Firm_user_services"
  OWNER TO postgres;

CREATE TABLE public."Firms"
(
  id integer DEFAULT nextval('firms_id_seq'::regclass),
  name character varying(150) NOT NULL,
  user_id integer NOT NULL,
  country_id integer NOT NULL,
  city_id integer NOT NULL,
  image character varying(255),
  email character varying(150) NOT NULL,
  phone character varying(150) NOT NULL,
  slug character varying(255) NOT NULL,
  description text,
  "order" integer default 100,
  showRecomm boolean default false,
  errors text,
  recomm text,
  is_valid boolean default false,
  status integer default 1,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Firms"
  OWNER TO postgres;

CREATE TABLE public."Messages"
(
  id integer DEFAULT nextval('messages_id_seq'::regclass),
  user_from integer NOT NULL,
  user_to integer NOT NULL,
  subject character varying(255) NOT NULL,
  message text not null,
user_from_delete boolean default false,
user_to_delete boolean default false,
  date bigint NOT NULL,
  read_date integer,
  data json,
  type character varying(150) NOT NULL,
  substance character varying(150) NOT NULL,
  substance_id integer,
  substance_sub character varying(150),
  substance_sub_id integer,
  "group" integer,
  is_new boolean default true,
  is_show boolean default false,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Messages"
  OWNER TO postgres;

CREATE TABLE public."Rights_blocks"
(
  id integer DEFAULT nextval('rights_blocks_id_seq'::regclass),
  title character varying(255) NOT NULL,
  slug character varying(255),
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Rights_blocks"
  OWNER TO postgres;

CREATE TABLE public."Tours"
(
  id integer DEFAULT nextval('tours_id_seq'::regclass),
  title character varying(255) NOT NULL,
user_id integer not null,
firm_id integer not null,
country_id integer,
city_id integer,
  image character varying(255) NOT NULL,
  slug character varying(255),
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Tours"
  OWNER TO postgres;


CREATE TABLE public."Users"
(
  id integer DEFAULT nextval('users_id_seq'::regclass),
  first_name character varying(150),
  last_name character varying(150),
  avatar character varying(150),
  sex integer,
  password character varying(255) not null,
  salt character varying(255) not null,
  email character varying(255) not null,
  role_id integer,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Users"
  OWNER TO postgres;

CREATE TABLE public."Users_rights"
(
  id integer DEFAULT nextval('users_rights_id_seq'::regclass),
  user_id integer not null,
  right_id character varying(150) not null,
  "create" boolean default false,
  "read" boolean default false,
  update_action boolean default false,
  delete_action boolean default false,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Users_rights"
  OWNER TO postgres;

CREATE TABLE public."Сities"
(
  id integer DEFAULT nextval('cities_id_seq'::regclass),
  name character varying(150) not null,
  country_id integer not null,
  "createdAt" date,
  "updatedAt" date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Сities"
  OWNER TO postgres;
