let Sequelize = require('sequelize');
let bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  let Users = sequelize.define('Users', {
    first_name: {
      type: Sequelize.STRING,
      //field: 'first_name' // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    last_name: {
      type: Sequelize.STRING,
    },
    avatar: {
      type: Sequelize.STRING,
    },
    sex: {
      type: Sequelize.INTEGER,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    salt: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: {
          args: true,
          msg: 'Please enter a email address'
        }
      }
    },
    role_id: {
      type: Sequelize.INTEGER
    },
  }, {
    hooks: {
      beforeValidate: function () {
      },

      afterValidate: function (user) {
        if (user.password) {
          user.password = Users.encryptPasswor(`${user.password}${user.salt}`);
        }
      }
    },
    freezeTableName: true // Model tableName will be the same as the model name
  });

  Users.encryptPassword = (password) => {
    return bcrypt.hashSync(password, 8);
  };

  Users.verifyPassword = (userPassword, userSalt, password) => {
    console.log('verifyPassword', userPassword, Users.encryptPassword( `${password}${userSalt}` ), `${password}${userSalt}` );
    return bcrypt.compareSync( `${password}${userSalt}`, userPassword);
  };


  return Users;
};