let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  _ = require('underscore'),
  queue = require('queue');
const moduleKey = 'messages';

router.get(
  `/${moduleKey}/:substance/:substance_id/:substance_sub?/:substance_sub_id?`,
  passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const db = req.app.get('db');
    const userId = +req.user.id;
    let data = [];
    let dataUsers = [];
    const q = queue({concurrency: 1});
    const {substance, substance_id, substance_sub, substance_sub_id} = req.params;

    q.push(next => {
      getMessages(db, substance, substance_id, substance_sub, substance_sub_id)
        .then(res => {
          data = res;
          next();
        })
        .catch(error => {
          next(error);
        });
    });

    // Обновляем статус записей как не новое
    q.push(next => {
      let where = {substance, substance_id, user_to: userId };
      if(substance_sub)where['substance_sub'] = substance_sub;
      if(substance_sub_id)where['substance_sub_id'] = substance_sub_id;

      // substance, substance_id, substance_sub, substance_sub_id
      db.Messages.update({is_new: false}, {where})
        .then(res => {
          next();
        })
        .catch(error => {
          next(error);
        });
    });

    q.start(error => {
      if (!error || error !== 'undefined') res.send({status: 200, data});
      else res.send({status: 500, error});
    });
  }
);

router.post(
  `/${moduleKey}/:substance?/:substance_id?/:substance_sub?/:substance_sub_id?`,
  passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const db = req.app.get('db');
    const userId = +req.user.id;
    const data = req.body;
    const q = queue({concurrency: 1});
    let cout;
    const {substance, substance_id, substance_sub, substance_sub_id} = req.params;

    data.user_from = userId;
    data.date = new Date();

    q.push(next => {
      db.Messages.create(data)
        .then((res) => {
          cout = data;
          next();
        })
        .catch(error => {
          next(error);
        });
    });

    if (substance && substance_id) {
      q.push(next => {
        getMessages(db, substance, substance_id, substance_sub, substance_sub_id)
          .then(list => {
            cout = list;
            next();
          })
          .catch(error => {
            next(error);
          });
      });
    }

    q.start(error => {
      if (!error) res.send({status: 200, data: cout});
      else res.send({status: 500, error});
    });
  }
);

getMessages = (db, substance, substance_id, substance_sub = '', substance_sub_id = 0) => {
  const q = queue({concurrency: 1});
  return new Promise((solved, reject) => {
    q.push(next => {
      let where = {substance, substance_id};
      if (substance_sub) where[substance_sub] = substance_sub;
      if (substance_sub_id) where[substance_sub_id] = substance_sub_id;

      db.Messages.findAll({
        where,
        order: [['date', 'desc'], ['id', 'desc']],
        raw: true
      })
        .then((res) => {
          data = res;
          next();
        })
        .catch(error => {
          next(error);
        })
    });

    q.push(next => {
      if (data.length) {
        const listUsers = [];
        data.forEach(item => {
          if (!listUsers[item.user_from]) listUsers.push(item.user_from);
          if (!listUsers[item.user_to]) listUsers.push(item.user_to);
        });

        db.Users.findAll({
          where: {id: listUsers},
          raw: true,
          attributes: ['id', 'login', 'avatar', 'sex', 'first_name', 'last_name']
        })
          .then(users => {
            const usersGroup = _.groupBy(users, item => item.id);
            data.forEach(item => {
              item.user_from = usersGroup[item.user_from][0];
              item.user_to = usersGroup[item.user_to][0];
            });
            next();
          })
          .catch(error => {
            next(error);
          })
      } else {
        next();
      }
    });

    q.start(error => {
      if (!error) {
        solved(data);
      } else {
        reject(error);
      }
    });
  });
};

module.exports = router;