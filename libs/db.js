let fs = require('fs');
let path = require('path');
let Sequelize = require('sequelize');

let sequelize = new Sequelize('wtravel', 'postgres', 'postgres', {
  host: 'localhost',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});
let db = {};

fs.readdirSync('./models').forEach((file) => {
  let model = sequelize['import'](path.join('../models', file));
  db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});
db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.getSlug = (title) =>{
};

module.exports = db;