let constants = require('../constants');

module.exports = {
  list: {
    'common': {
      description: {
        rule: [constants.NOT_NULL],
        error: constants.FIELD_MUST_FILLED,
        require: true
      }
    },
    'images': {
      images: {
        inTable: {
          table: 'Files',
          where: {section: 'firms-page', item_id: '#itemId'}
        },
        require: true,
        error: constants.TABLE_NEED_ADD_ITEM
      }
    }
  }
};