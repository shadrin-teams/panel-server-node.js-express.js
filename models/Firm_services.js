let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let Firm_services = sequelize.define('Firm_services', {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }
    }
  );

  return Firm_services;
};