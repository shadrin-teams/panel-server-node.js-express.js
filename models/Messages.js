let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Messages', {
      user_from: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      user_to: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      subject: {
        type: Sequelize.STRING,
        allowNull: false
      },
      message: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      user_from_delete: {
        type: Sequelize.BOOLEAN
      },
      user_to_delete: {
        type: Sequelize.BOOLEAN
      },
      date: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      read_date: {
        type: Sequelize.INTEGER
      },
      data: {
        type: Sequelize.JSON
      },
      type: {
        type: Sequelize.ENUM,
        values: ['message', 'validation']
      },
      substance: {
        type: Sequelize.ENUM,
        values: ['firm', 'tour']
      },
      substance_id: {
        type: Sequelize.INTEGER
      },
      substance_sub: {
        type: Sequelize.ENUM,
        values: ['page', 'service']
      },
      substance_sub_id: {
        type: Sequelize.INTEGER
      },
      group: {
        type: Sequelize.INTEGER
      },
      is_new: {
        type: Sequelize.BOOLEAN
      },
      is_show: {
        type: Sequelize.BOOLEAN
      }
    }
  );
};