let express = require('express'),
  router = express.Router(),
  moduleKey = '',
  passport = require('passport'),
  fs = require('fs'),
  multiparty = require('multiparty'),
  im = require('imagemagick'),
  queue = require('queue'),
  model = {};

router.post('/' + moduleKey + '/:cat/:id', passport.authenticate('jwt', {session: false}), (req, res, next) => {

  const db = req.app.get('db');
  const category = req.params.cat;
  const itemId = req.params.id;

  console.log('post');
  // создаем форму
  let form = new multiparty.Form();
  //здесь будет храниться путь с загружаемому файлу, его тип и размер
  let uploadFile = {uploadPath: '', type: '', size: 0};
  //максимальный размер файла
  let maxSize = 2 * 1024 * 1024; //2MB
  //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
  let supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
  //массив с ошибками произошедшими в ходе загрузки файла
  let errors = [];

  //если произошла ошибка
  form.on('error', (err) => {
    if (fs.existsSync(uploadFile.path)) {
      //если загружаемый файл существует удаляем его
      fs.unlinkSync(uploadFile.path);
      console.log('error');
    }
  });

  form.on('close', () => {
    //если нет ошибок и все хорошо
    if (errors.length === 0) {

      let q = queue({concurrency: 1}),
        itemModel = {},
        itemCountImage = 0,
        uploadError = '',
        params = '',
        file = '',
        dir = '',
        fileName = '',
        dirName = '',
        fileDtr = '',
        smallFileDtr = '',
        bigFileDtr = '';

      if (!category || !itemId) uploadError = 'Upload image error: have\'t category or id item';

      console.log('_1');
      // Выставкиваем модель по ID и категории
      q.push((next) => {
        console.log('_2');
        if (!uploadError) {
          console.log('_3');
          let modelName = '';
          switch (category) {
            case 'post' :
              modelName = 'Posts';
              break;
            default :
              modelName = 'Posts';
          }
          console.log('_4');
          model = require('../../models/' + modelName);
          model.findOne({id: itemId}, (err, data) => {
            if (!err) {
              if (data && data.id) itemModel = data;
              else uploadError = 'incorrect ID or category - ' + modelName + ' - ' + itemId;
            } else {
              uploadError = err;
            }
            next();
          });
        }
        else next();
      });

      // создаем категории
      q.push((next) => {
        console.log('_5');
        if (!uploadError) {
          console.log('_6');
          params = uploadFile.path.split('/');
          file = params[params.length - 1];
          params[params.length - 1] = '';
          dir = '/upload/';

          try {
            fs.mkdirSync('./public' + dir + category + '/' + itemId);
          } catch (err) {
            if (err) {

              try {
                console.error('./public' + dir + category);
                fs.mkdirSync('./public' + dir + category);
                console.error('./public' + dir + category + '/' + itemId);
                fs.mkdirSync('./public' + dir + category + '/' + itemId);
              }
              catch (err) {
                console.log('err', err);
                //uploadError = 'problem with create folder';
              }
            }
          }

          dir += category + '/' + itemId;
          next();
        }
        else next();
      });

      // Закачиваем оригинальный файл
      q.push((next) => {
        console.log('push 1');
        if (!uploadError) {
          fs.rename(uploadFile.path, './public' + dir + '/' + file, (err) => {
            console.log('push 2');
            if (err) uploadError = err;
            else {
              fileName = file;
              dirName = dir + '/';
              fileDtr = './public' + dir + '/' + file;
              smallFileDtr = './public' + dir + '/thumb_' + file;
              bigFileDtr = './public' + dir + '/big_' + file;
            }

            next();
          })
        }
        else next();

      });

      // Закачиваем большой файл 800x600
      q.push((next) => {
        console.log('push 3');
        if (!uploadError) {
          im.convert(
            [fileDtr, '-resize', '800x600', fileDtr],
            (err, stdout) => {
              if (err) {
                uploadError = err;
                console.log('push 3 ERROR', err);
              }
              next();
            }
          )
        }
        else next();
      });

      // Закачиваем маленький файл 85x85
      q.push((next) => {
        console.log('push 4');
        if (!uploadError) {
          im.convert(
            [fileDtr, '-resize', '85x85', smallFileDtr],
            (err, stdout) => {
              if (err) uploadError = err;
              next();
            }
          )
        }
        else next();
      });

      // Закачиваем маленький файл 350x380
      q.push((next) => {
        if (!uploadError) {
          im.convert(
            [fileDtr, '-resize', '350x380', bigFileDtr],
            (err, stdout) => {
              if (err) uploadError = err;
              next();
            }
          )
        }
        else next();
      });

      // Сохраняем в галлерею
      q.push((next) => {
        if (!uploadError) {
          let image = new galleries();
          image.dir = dirName.replace('../', '').replace('public/', '');

          image.image = fileName;
          image.category = category;
          image.item = itemId;
          image.save()
            .then((data) => {
              next();

            }, (err) => {
              uploadError = err;
              next();
            })
        }
        else next();

      });

      // Проверяем солько уже фотографий у это продукции
      q.push((next) => {
        if (!uploadError) {
          galleries.count({category: category, item: itemId}, (error, count) => {
            if (!error) {
              itemCountImage = count;
            }
            next();
          })

        } else next();
      });

      /*            // Если у продукции нет картинки сохраняем первую картинки в поле image
       q.push((next) => {
       console.log("push 9", itemCountImage);
       // Сохраняемкартинку только если это первая картинки в галларее
       if (!uploadError && itemCountImage === 1) {
       itemModel.image = {
       file: fileName,
       dir: dirName.replace("../", "").replace("public/", "")
       };
       itemModel.save()
       .then((data) => {
       next();
       }, (error) => {
       uploadError = error;
       next();
       })

       } else next();
       });*/

      q.start((err) => {
        console.log('push finish');
        if (!uploadError) {

          let image = new galleries();
          image.dir = dirName.replace('../', '').replace('public/', '');

          image.image = fileName;
          image.category = category;
          image.item = itemId;
          image.save()
            .then((data) => {
              //сообщаем что все хорошо
              res.send({
                status: 'ok',
                text: 'Success',
                file: {
                  image: fileName,
                  dir: dirName.replace('../', '').replace('public/', '')
                }
              });
            })


        } else {
          res.status(500).send({
            status: 'fail',
            error: uploadError
          });
        }
      });
    }
    else {
      if (fs.existsSync(uploadFile.path)) {
        //если загружаемый файл существует удаляем его
        fs.unlinkSync(uploadFile.path);
      }
      //сообщаем что все плохо и какие произошли ошибки
      res.send({status: 'bad', errors: errors});
    }
  });

  // при поступление файла
  form.on('part', (part) => {

    //читаем его размер в байтах
    uploadFile.size = part.byteCount;
    //читаем его тип
    uploadFile.type = part.headers['content-type'];
    //путь для сохранения файла
    uploadFile.path = './public/upload/' + part.filename;

    //проверяем размер файла, он не должен быть больше максимального размера
    if (uploadFile.size > maxSize) {
      errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
    }

    //проверяем является ли тип поддерживаемым
    if (supportMimeTypes.indexOf(uploadFile.type) === -1) {
      errors.push('Unsupported mimetype ' + uploadFile.type);
    }

    //если нет ошибок то создаем поток для записи файла
    if (errors.length === 0) {
      let out = fs.createWriteStream(uploadFile.path);
      part.pipe(out);
    }
    else {
      //пропускаем
      //вообще здесь нужно как-то остановить загрузку и перейти к onclose
      part.resume();
    }
  });

  // парсим форму
  form.parse(req);

});

module.exports = router;