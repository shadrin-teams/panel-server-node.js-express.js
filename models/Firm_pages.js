let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let Firm_pages = sequelize.define('Firm_pages', {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      slug: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }
    }
  );

  return Firm_pages;
};