let queue = require('queue');
let _ = require('underscore');
let firmRule = require('./rules/firm.rules');
let firmPageRule = require('./rules/firm.page.rules');
let firmServiceRule = require('./rules/firm.service.rules');
let constants = require('./constants');

class PublishHelper {
  constructor() {
    this.rules = {};
    this.db;
  }

  /*validateAndSave(db, table, itemId) {
    const q = queue({constants: 1});

    return new Promise((solved, reject) => {
      q.push(next => {
        this.validate(db, table, itemId)
          .then(data => {
            next();
          })
          .catch(error => {
            next(error);
          });
      });

      q.push(next => {
        // Тут будет изминение статуса
      });

      q.start(error => {
        if (!error) solved();
        else reject();
      });
    });
  }*/

  validate(db, table, itemId = null, needSave = false) {
    this.db = db;
    return new Promise((solved, reject) => {
      let rules = {
        'Firms': firmRule,
        'Firm_user_pages': firmPageRule,
        'Firm_user_services': firmServiceRule
      };

      const q = queue({concurrency: 1});
      let data = {};
      let require = [];
      let errors = [];
      let recommendations = [];

      q.push(next => {
        this.db[table].findOne({
          where: {id: itemId},
          raw: true
        })
          .then(res => {
            data = res;
            next();
          })
          .catch(error => {
            errors.push(error);
            next(error);
          })
      });

      if (rules[table]) {
        const tableRules = rules[table].list;
        Object.keys(tableRules).forEach((group, index) => {
          Object.keys(tableRules[group]).forEach((field) => {
            const fieldObj = tableRules[group][field];
            if ('rule' in fieldObj) {
              q.push(next => {
                this.checkField(fieldObj.rule, data, group, field)
                  .then(res => {
                    next();
                  })
                  .catch(error => {
                    if (fieldObj.require) {
                      errors = errors.concat(error);
                    } else {
                      recommendations = recommendations.concat(error);
                    }
                    next();
                  })
              });
            }

            if ('inTable' in fieldObj) {
              q.push(next => {
                this.checkInTable(fieldObj.inTable, {itemId}, group, field)
                  .then(res => {
                    next();
                  })
                  .catch(error => {
                    if (fieldObj.require) {
                      errors = _.union(errors, error);
                    } else {
                      recommendations = _.union(recommendations, error);
                    }
                    next();
                  })
              });
            }
          });
        });

        if (needSave) {
          q.push(next => {
            db[table].update(
              {
                recomm: recommendations.length ? JSON.stringify(_.groupBy(recommendations, item => item.group)) : '',
                errors: errors.length ? JSON.stringify(_.groupBy(errors, item => item.group)) : '',
                is_valid: !errors.length
              },
              {where: {id: itemId}}
            )
              .then(data => {
                next();
              })
              .catch(error => {
                next(error);
              });
          });
        }

        q.start((error) => {
          let cout = {};
          if (recommendations.length) {
            cout.recommendations = _.groupBy(recommendations, item => item.group);
          }

          if (!errors.length) {
            cout.valid = true;
            solved(cout);
          } else {
            cout.errors = _.groupBy(errors, item => item.group);
            cout.valid = false;
            reject(cout);
          }
        });
      } else {
        reject();
      }
    });
  }

  checkInTable(tableParams, params, group, field) {
    let errors = [];
    return new Promise((solved, reject) => {
      const where = {};
      Object.keys(tableParams.where).forEach(key => {
        const value = tableParams.where[key];
        where[key] = typeof value === 'string' ? value.replace('#itemId', params.itemId) : value;
      });

      this.db[tableParams.table].count({
        where,
        raw: true
      })
        .then(count => {
          if (tableParams.min && count < tableParams.min) {
            errors.push({
              group,
              field,
              error: tableParams.min === 1 ? constants.TABLE_NEED_ADD_ITEM : constants.TABLE_NEED_ADD_MIN_ITEM
            });
            reject(errors);
          } else {
            solved();
          }

        })
        .catch(error => {
          throw new Error(error)
        });
    });
  }

  checkField(rule, data, group, field) {
    let errors = [];
    return new Promise((solved, reject) => {
      rule.forEach(item => {
        switch (item) {
          case constants.NOT_NULL:
            if (!data[field]) errors.push({
              group,
              field,
              error: constants.FIELD_MUST_FILLED
            });
            break;
        }
      });
      if (!errors.length) {
        solved();
      } else {
        reject(errors);
      }
    })
  }
}

module.exports = new PublishHelper();