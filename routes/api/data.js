let express = require('express'),
  router = express.Router(),
  passport = require('passport');

const moduleKey = 'data';

router.get(`/${moduleKey}/countries`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');

  db.Countries.findAll({attributes: ['id', 'name']})
    .then(items => {
      res.send({status: 200, data: {items, count: items.length}});
    })
    .catch(error => {
      console.log('error', error);
      res.status(200).send({status: 500, error});
    });
});

router.get(`/${moduleKey}/cities/:countryId?`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const country_id = req.params.countryId || '';

  db.Сities.findAll({
    where: {country_id},
    attributes: ['id', 'name', 'country_id']
  })
    .then(items => {
      res.send({status: 200, data: {items, count: items.length}});
    })
    .catch(error => {
      res.status(200).send({status: 500, error});
    });
});

module.exports = router;