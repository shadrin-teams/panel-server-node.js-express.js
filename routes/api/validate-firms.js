let express = require('express'),
  passport = require('passport'),
  router = express.Router(),
  queue = require('queue');
const moduleKey = 'validate-firms';

router.get(`/${moduleKey}`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');

  db.Firm_moderations.findAndCountAll({
    raw: true
  })
    .then(data => {
      res.send({status: 200, data: {items: data.rows, count: data.count}});
    })
    .catch(error => {
      res.send({status: 500, error});
    });
});

module.exports = router;