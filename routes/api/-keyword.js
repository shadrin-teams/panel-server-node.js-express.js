let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  moduleKey = 'keyword',
  queue = require('queue');

/**
 * Вызвращает список продукции
 *
 */

router.get('/' + moduleKey + '/:sectionId', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let sectionId = req.params.sectionId;
  if (sectionId) {
    let model = require('../../models/Keywords');
    model.find({section: sectionId})
      .sort({value:-1})
      .then((data) => {
        res.send({status: 'ok', data: data});
      })
      .catch((error) => {
        res.send({status: 'error', error});
      });
  } else {
    res.send({status: 'error', error: 'No have section id'});
  }

});

router.get(
  '/' + moduleKey + '/:sectionId/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {
    let id = req.params.id;
    if (id) {

      let model = require('../../models/Keywords');
      model.findOne({id: id})
        .then(function(items) {
          res.send({data: items, status: 'ok'});
        })
        .catch(function(error) {
          res.send({error, status: 'fail'});
        });

    }
    else
      res.send({data: '', error: 'no category', status: 'fail'});
  })
;

/**
 * Удаление
 */

router.delete('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id;
  if (id) {
    let model = require('../../models/Keywords');
    model.remove({id}, function(err) {
      if (!err) res.send({status: 'deleted'});
      else res.send({status: false, error: err});
    })

  }
  else res.send({data: '', error: 'no id'});

});

router.post(
  '/' + moduleKey + '/:siteId' + '/:sectionId', passport.authenticate('jwt', {session: false}),
  function(req, res, next) {

    let data = req.body.data;
    let siteId = req.params.siteId;
    let sectionId = req.params.sectionId;

    if (data && siteId && sectionId) {
      let model = require('../../models/Keywords');
      let Model = new model();
      Model.id = Model._id;

      Object.keys(data).forEach((field) => {
        Model[field] = data[field];
      });
      Model.site = siteId;
      Model.section = sectionId;

      Model.save(function(err) {

        if (err) {
          res.send({error: err, status: 'fail'});
        } else {
          res.send({data: Model, status: 'ok'});
        }

      });
    }
    else
      res.send({data: '', error: 'No data'});

  }
);

router.put('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id,
    data = req.body.data;

  if (data && data.id) {

    let model = require('../../models/Keywords');
    model.findById(data.id, (error, Model) => {
      if (data) {

        Object.keys(data).forEach((field) => {
          Model[field] = data[field];
        });

        Model.save((error) => {
          if (!error) {
            res.send({data: {Model}, status: 'ok'});
          } else {
            res.send({error, status: 'fail'});
          }
        });
      }
      else {
        res.send({error: 'incorrect id', status: 'fail'});
      }
    });
  } else {
    res.send({error: 'incorrect data', status: 'fail'});
  }

});

module.exports = router;