let express = require('express');
let router = express.Router();
const moduleKey = 'errors';

router.post('/' + moduleKey, function(req, res, next) {
  const db = req.app.get('db');
  const params = req.body;
  params.data = JSON.stringify(params.data);

  if(params.error && params.errorType){
    db.Errors.create(params)
      .then(() => {
        res.send({status: 200});
      })
      .catch(error => {
        res.status(500).send({status: 500, error});
      })
  } else {

    res.status(500).send({status: 500, error: 'Error is required'});
  }

});

module.exports = router;