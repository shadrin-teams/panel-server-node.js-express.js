var express = require('express');
var router = express.Router();
var moduleKey = 'Admins';
var Admins = require('../../models/Admins');
var passport = require('passport');

router.get('/'+moduleKey, function(req, res, next) {

    Admins.find(function(err, data) {
        res.json(data);
    });
});

router.post('/'+moduleKey, function(req, res, next) {
    res.json('POST ABOUT');
});

module.exports = router;