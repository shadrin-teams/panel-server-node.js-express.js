let express = require('express'),
  moment = require('moment'),
  _ = require('lodash'),
  moduleKey = 'refreshToken',
  router = express.Router(),
  tokenData,
  db,
  test;

router.get('/' + moduleKey, (req, res, next) => {

  const tokenAttr = req.headers.authorization ? req.headers.authorization.split(' ') : null;

  if (tokenAttr && tokenAttr.length === 2) {
    const token = tokenAttr[1];
    db = req.app.get('db');
    // Check the existence of the token
    db.Access_tokens.findAll({
      where: {token},
      attributes: ['id', 'token', 'ip', 'exp_date', 'user_id']
    })
      .then(data => {
        console.log('3', token, data);
        if (!data.length) {
          res.status(403).send({status: 403, data: []});
        } else {
          tokenData = _.clone(data[0].dataValues);

          // Check correspondence of IP
          let ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

          if (ip === '::1' || ip === '::ffff:127.0.0.1') ip = '127.0.0.1';
          if (tokenData.ip === ip) {

            // Updates the token expired date, if all is ok
            db.Access_tokens
              .update(
                {exp_date: +moment().add(3, 'hours').unix()},
                {where: {id: tokenData.id}}
              )
              .then(data => {
                next();
              })
              .catch(error => {
                throw new Error(error);
              });

          } else {
            console.info('Delete', tokenData.ip , '===', ip);
            // Delete token
            db.Access_tokens
              .destroy({where: {id: tokenData.id}});

            res.status(403).send({status: 403});
          }

        }
      })
      .catch(error => {
        console.error(error);
        res.status(500).send({status: 500, error: error});
      });
  } else {
    res.status(500).send({status: 500});
  }
});

router.get('/' + moduleKey, (req, res, next) => {
  db.Users.findOne({where: {id:tokenData.user_id}})
    .then(user => {
      res.json({
        'status': 200,
        'token': tokenData.token,
        'token_exp': moment().add(3, 'h'),
        'user': {
          'id': user.id,
          'email': user.email,
          'role': user.role_id,
          first_name: user.first_name,
          last_name: user.last_name,
          avatar: user.avatar,
          sex: user.sex,
          rights: user.rights
        }
      });
    })
    .catch(error => {
      console.log(error);
    })
});

module.exports = router;