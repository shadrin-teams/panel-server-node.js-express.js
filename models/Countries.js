let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Countries', {
      name: {
        type: Sequelize.STRING,
        allowNull: false
      }
    }
  );
};