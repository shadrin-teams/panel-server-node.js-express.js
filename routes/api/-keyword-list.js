let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  queue = require('queue'),
  moduleKey = 'keyword-list';

/**
 * Вызвращает список продукции
 *
 */

router.post(
  '/' + moduleKey + '/:siteId' + '/:sectionId', passport.authenticate('jwt', {session: false}),
  function(req, res, next) {

    let data = req.body.data;
    let siteId = req.params.siteId;
    let sectionId = req.params.sectionId;
    let countAdd = 0;
    let errorAdd = {};

    if (data && siteId && sectionId) {
      let model = require('../../models/Keywords');

      let q = queue({concurrency: 1});
      for (let i = 0; i < data.length; i++) {

        q.push((next) => {
          let Model = new model();
          Model.id = Model._id;
          Model.name = data[i].name;
          Model.value = data[i].value;
          Model.site = siteId;
          Model.section = sectionId;

          Model.save(function(err) {
            if(!err){
              countAdd ++;
            } else {
              errorAdd[Model.id] = err;
            }

            next();
          });
        });

      }

      q.start((error) => {
        if (errorAdd.length) {
          res.send({error: error, status: 'fail', data: errorAdd});
        }
        else {
          res.send({status: 'ok', countAdd: countAdd, errors:errorAdd});
        }
      });
    }
    else
      res.send({data: '', error: 'No data'});

  }
);

module.exports = router;