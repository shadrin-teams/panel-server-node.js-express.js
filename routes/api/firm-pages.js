let express = require('express'),
  router = express.Router(),
  passport = require('passport');

const moduleKey = 'firm-pages';

router.get(`/${moduleKey}`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  db.Firm_pages.findAll({
    attributes: ['id', 'name', 'slug'],
    order: [['name']]
  })
    .then((items) => {
      res.send({status: 200, items});
    })
    .catch(error => {
      res.send({status: 500, error});
    })
});

module.exports = router;