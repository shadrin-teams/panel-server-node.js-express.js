var mongoose = require('mongoose');
var crypto = require('crypto');

// User
var User = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

User.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
  //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512);
};

User.methods.verifyPassword = function(password) {
  console.log('verifyPassword', password, this.encryptPassword(password));
  return this.password !== this.encryptPassword(password);
};

/*
 User.virtual('userId')
 .get(function () {
 return this.id;
 });

 User.virtual('password')
 .set(function(password) {
 this._plainPassword = password;
 this.salt = crypto.randomBytes(32).toString('base64');
 //more secure - this.salt = crypto.randomBytes(128).toString('base64');
 this.hashedPassword = this.encryptPassword(password);
 })
 .get(function() { return this._plainPassword; });


 User.methods.checkPassword = function(password) {
 return this.encryptPassword(password) === this.hashedPassword;
 };*/

module.exports = mongoose.model('User', User);