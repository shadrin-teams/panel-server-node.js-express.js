let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  moduleKey = 'site',
  queue = require('queue');

/**
 * Вызвращает список продукции
 *
 */

router.get('/' + moduleKey, passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let model = require('../../models/Sites');
  model.find()//(err, data) => {
  //model.loadRelations(data)
    .then((data) => {
      res.send({status: 'ok', data: data});
    })
    .catch((error) => {
      console.log(error);
      res.send({status: 'error', error});
    });
  //})

});

router.get('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let id = req.params.id;
  if (id) {

    let model = require('../../models/Sites');
    model.findOne({id: id})
      .then(function(items) {
        res.send({data: items, status: 'ok'});
      })
      .catch(function(error) {
        console.log(error);
        res.send({error, status: 'fail'});
      });


    /*model.findOne({id: id}, function(err, data) {
     model.loadRelations([data])
     .then(function(items) {

     if (err) res.send({error: err});
     else res.send({data: items[0], status: 'ok'});
     })
     .catch(function(error) {
     console.log(error);
     res.send({error, status: 'fail'});
     });
     })*/

  }
  else
    res.send({data: '', error: 'no category', status: 'fail'});
})
;

/**
 * Удаление
 */

router.delete('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id;
  if (id) {
    let model = require('../../models/Sites');
    model.remove({id: id}, function(err) {
      if (!err) res.send({status: 'deleted'});
      else res.send({status: false, error: err});
    })

  }
  else res.send({data: '', error: 'no id'});

});

router.post('/' + moduleKey, passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let data = req.body.data;

  if (data) {
    let model = require('../../models/Sites');
    let Model = new model();
    Model.id = Model._id;

    Object.keys(data).forEach((field) => {
      if (field === 'category' && typeof data[field] === 'object' && data[field].id) {
        Model[field] = data[field].id;
      } else {
        Model[field] = data[field];
      }
    });
    console.log('add Model', Model);
    Model.save(function(err) {

      if (err) {
        console.log('add err', err, Model);
        res.send({error: err});
      }
      else {
        console.log('add ok', Model);
        res.send({data: Model.id, status: true});
      }

    });
  }
  else
    res.send({data: '', error: 'No data'});

});

router.put('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id,
    data = req.body.data;

  if (data && data.id) {

    let model = require('../../models/Sites');
    model.findById(data.id, (error, Model) => {
      if (data) {
        /*
         Object.keys(data).forEach((field) => {
         if (field === 'category' && typeof data[field] === 'object' && data[field].id) {
         Model[field] = data[field].id;
         } else {
         Model[field] = data[field];
         }
         });
         Model.save()
         .then((data) => {
         model.loadRelations([data])
         .then((item) => {
         console.log(1);
         res.send({data: {item: item[0]}, status: 'ok'});
         })
         .catch((error) => {
         console.log(2);
         res.send({error, status: 'fail'});
         })

         }, (error) => {
         console.log('edit err', error, Model);
         res.send({error, status: 'fail'});
         });*/

        Object.keys(data).forEach((field) => {
          Model[field] = data[field];
        });

        Model.save((error) => {
          if (!error) {
            res.send({data: {Model}, status: 'ok'});
          } else {
            res.send({error, status: 'fail'});
          }
        });
      }
      else {
        res.send({error: 'incorrect id', status: 'fail'});
      }
    });
  } else {
    res.send({error: 'incorrect data', status: 'fail'});
  }

});

module.exports = router;