let express = require('express'),
  router = express.Router(),
  moduleKey = 'files',
  passport = require('passport'),
  moment = require('moment'),
  _ = require('lodash'),
  fs = require('fs'),
  multiparty = require('multiparty'),
  im = require('imagemagick'),
  queue = require('queue'),
  helper = require('../../libs/helper'),
  config = require('../../config/main'),
  model = {};

router.get(`/${moduleKey}/main/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const userID = req.user.id;
  const id = +req.params.id;
  let fileObj = {};
  let listFiles = [];
  const q = queue({concurrency: 1});

  q.push(next => {
    // Find file, and check access
    db.Files.findOne({
      where: {
        user_id: userID,
        id
      }
    })
      .then(file => {
        if (file) {
          fileObj = file;
          return db.Files.update(
            {main: false},
            {
              where: {
                section: fileObj.section,
                user_id: userID,
                item_id: fileObj.item_id
              }
            }
          )
        } else {
          next();
        }
      })
      .then(file => {
        next();
      })
      .catch(error => {
        next(error);
      })
  });

  // Выделяем указынный файл
  q.push(next => {
    if (fileObj.id) {
      db.Files.update(
        {main: true},
        {where: {user_id: userID, id: fileObj.id}}
      )
        .then(data => {
          next();
        })
        .catch(error => {
          next(error);
        });

    } else {
      next()
    }
  });

  // Делаем новую выборку файлов
  q.push(next => {
    db.Files.findAll({
      where: {user_id: userID, section: fileObj.section, item_id: fileObj.item_id},
      order: [
        ['order', 'asc'],
        ['id', 'desc']
      ]
    })
      .then(data => {
        listFiles = data;
        next();
      })
      .catch(error => {
        next(error);
      });
  });

  q.start(error => {
    if (!error && listFiles.length) {
      res.send({status: 200, items: listFiles});
    } else {
      res.send({status: 500, error});
    }
  });

});

router.get(
  `/${moduleKey}/:section/:itemId/:type?`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const db = req.app.get('db');
    const section = req.params.cat;
    const itemId = +req.params.itemId;
    const type = req.params.type;
    let where = {
      section,
      item_id: itemId
    };
    if (type) {
      where['type'] = type;
    }

    db.Files.findAll({
      where
    })
      .then(data => {
        res.send({status: 200, data});
      })
      .catch(error => {
        res.status(200).send({status: 500, error});
      })
  });

router.post(`/${moduleKey}/sort`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const data = req.body;
  const userId = +req.user.id;
  const q = queue();

  data.forEach(file => {
    q.push(next => {
      db.Files.update(
        {order: file.order},
        {where: {user_id: userId, id: file.id}}
      )
        .then(data => {
          next();
        })
        .catch(error => {
          next(error);
        });
    });
  });

  q.start(error => {
    if (!error) {
      res.send({status: 200});
    } else {
      res.send({status: 500, error});
    }
  });

});

router.post(`/${moduleKey}/:cat/:itemId/:type?`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const category = req.params.cat;
  const itemId = +req.params.itemId;
  const userId = +req.user.id;
  const fileType = req.params.type;

  // создаем форму
  let form = new multiparty.Form();
  //здесь будет храниться путь с загружаемому файлу, его тип и размер
  let uploadFile = {uploadPath: '', type: '', size: 0};
  //максимальный размер файла
  let maxSize = 8 * 1024 * 1024; //2MB
  //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
  let supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
  //массив с ошибками произошедшими в ходе загрузки файла
  let errors = [];
  const publicDir = 'public/';
  const rootDir = 'files/';

  //если произошла ошибка
  form.on('error', (err) => {
    if (fs.existsSync(uploadFile.path)) {
      //если загружаемый файл существует удаляем его
      fs.unlinkSync(uploadFile.path);
    }
  });

  if (!category) errors.push('API.files: CATEGORY_IS_REQUIRED');

  form.on('close', () => {
    //если нет ошибок и все хорошо
    if (errors.length === 0) {

      let q = queue({concurrency: 1}),
        itemModel = {},
        itemCountImage = 0,
        uploadError = '',
        params = '',
        file = '',
        dir = '',
        fileName = '',
        dirName = '',
        fileDtr = '',
        smallFileDtr = '',
        bigFileDtr = '',
        newFileId = 0;

      // Created directory
      q.push((next) => {
        if (!uploadFile.path || !uploadFile.type || !uploadFile.size) {
          errors.push('File is did`t upload');
        }

        if (!errors.length) {
          params = uploadFile.path.split('/');
          let fileParam = params[params.length - 1].split('.');
          file = `${moment().unix()}_${_.random(1000, 9999)}.${fileParam[fileParam.length - 1]}`;
          params[params.length - 1] = '';

          dir = `./${publicDir}${rootDir}${category}/`;
          try {
            fs.mkdirSync(dir);
          } catch (err) {
          }

          if (itemId) {
            dir += `${itemId}/`;
            try {
              fs.mkdirSync(dir);
            } catch (err) {
            }
          }

          next();
        }
        else next();
      });

      // Upload the original file
      q.push((next) => {
        if (!errors.length) {
          fs.rename(uploadFile.path, `${dir}/${file}`, err => {
            if (err) errors.push(err);
            else {
              fileName = file;
              dirName = `${dir}`;
              fileDtr = `${dir}${file}`;
              smallFileDtr = `${dir}thumb_${file}`;
              bigFileDtr = `${dir}big_${file}`;
            }

            next();
          })
        }
        else next();

      });

      // Закачиваем большой файл 800x600
      q.push((next) => {
        if (!errors.length) {
          im.convert(
            [fileDtr, '-resize', '800x600', fileDtr],
            (err, stdout) => {
              if (err) {
                errors.push(err);
              }
              next();
            }
          )
        }
        else next();
      });

      // Upload small size of image 85x85
      q.push((next) => {
        if (!errors.length) {
          im.convert(
            [fileDtr, '-resize', '85x85', smallFileDtr],
            (err, stdout) => {
              if (err) errors.push(err);
              next();
            }
          )
        }
        else next();
      });

      // Upload medium size of image 350x380
      q.push((next) => {
        if (!errors.length) {
          im.convert(
            [fileDtr, '-resize', '350x380', bigFileDtr],
            (err, stdout) => {
              if (err) uploadError = err;
              next();
            }
          )
        }
        else next();
      });

      // Saving in DB
      q.push((next) => {
        if (!errors.length) {
          let data = {
            folder: dirName.replace('../', '').replace(`./${publicDir}`, ''),
            file: fileName,
            section: category,
            item_id: itemId,
            user_id: userId,
            type: fileType
          };

          db.Files.create(data)
            .then(data => {
              newFileId = data.id;
              next();

            })
            .catch(err => {
              errors.push(err.original.error);
              next();
            });
        }
        else next();

      });

      q.start((err) => {
        if (!errors.length) {
          res.send({
            status: 200,
            text: 'Success',
            data: {
              id: newFileId,
              file: fileName,
              folder: dirName.replace('../', '').replace(`./${publicDir}`, '')
            }
          });

        } else {
          res.send({
            status: 500,
            error: uploadError
          });
        }
      });
    }
    else {
      if (fs.existsSync(uploadFile.path)) {
        //Delete file if we uploaded it
        fs.unlinkSync(uploadFile.path);
      }
      //сообщаем что все плохо и какие произошли ошибки
      res.send({status: 500, errors: errors.join(',')});
    }
  });

  // при поступление файла
  form.on('part', (part) => {
    //читаем его размер в байтах
    uploadFile.size = part.byteCount;
    //читаем его тип
    uploadFile.type = part.headers['content-type'];
    //путь для сохранения файла
    uploadFile.path = `${publicDir}${rootDir}${part.filename}`;

    //проверяем размер файла, он не должен быть больше максимального размера
    if (uploadFile.size > maxSize) {
      errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
    }

    //проверяем является ли тип поддерживаемым
    if (supportMimeTypes.indexOf(uploadFile.type) === -1) {
      errors.push('Unsupported mimetype ' + uploadFile.type);
    }

    //если нет ошибок то создаем поток для записи файла
    if (errors.length === 0) {
      let out = fs.createWriteStream(uploadFile.path);
      part.pipe(out);
    }
    else {
      //пропускаем
      //вообще здесь нужно как-то остановить загрузку и перейти к onclose
      part.resume();
    }
  });

  // парсим форму
  form.parse(req);

  if (errors.length) {
    res.status(500).send({error: errors.join(',')})
  }

});

router.delete(`/${moduleKey}/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  db.Files.findById(req.params.id)
    .then((res) => {
      try {
        fs.unlinkSync(`public/${res.dataValues.folder}thumb_${res.dataValues.file}`, (error) => {
          if (!error) {
          }
          /* else {
            res.send({status: 404, error})
          }*/
        });

        fs.unlinkSync(`public/${res.dataValues.folder}big_${res.dataValues.file}`, (error) => {
          if (!error) {
          }
          /*else {
            res.send({status: 404, error})
          }*/
        });

        fs.unlinkSync(`public/${res.dataValues.folder}${res.dataValues.file}`, (error) => {
          if (!error) {
          }
          /*else {
            res.send({status: 404, error})
          }*/
        });
      } catch (error) {
        // res.send({status: 404, error})
      }

      return db.Files.destroy({where: {id: req.params.id}});

    })
    .then((unlinkRes) => {
      res.send({status: 200, data: unlinkRes})
    })
    .catch((error) => {
      res.send({status: 404, error})
    });
});

router.put(`/${moduleKey}/:id`, passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const db = req.app.get('db');
  const id = +req.params.id;
  const data = req.body;
  const q = queue({concurrency: 1});
  let fileObj = {};

  if (data.item_id) {
    q.push(next => {
      db.Files.findOne({
        where: {id}
      })
        .then(file => {
          fileObj = file;
          next();
        })
        .catch(error => {
          next(error);
        });
    });

    q.push(next => {
      if (fileObj && !fileObj.item_id) {
        helper.moveFileInFolder(db, `${config.publicFolder}${fileObj.folder}`, `${config.publicFolder}${fileObj.folder}${data.item_id}`, fileObj.file);
        data.folder = `${fileObj.folder}${data.item_id}/`;
      }
      next();
    });
  }

  q.start(error => {
    db.Files.update(
      data,
      {where: {id}}
    )
      .then((file) => {
        console.log('res', file);
        res.send({status: 200, data: file})
      })
      .catch((error) => {
        res.send({status: 404, error})
      });
  });

});

module.exports = router;