let express = require('express'),
  router = express.Router(),
  _ = require('lodash'),
  moment = require('moment'),
  tokenData = {};

let refreshToken = require('./api/refreshToken'),
  errors = require('./api/errors'),
  auth = require('./api/auth'),
  messages = require('./api/messages'),
  files = require('./api/files'),
  firms = require('./api/firms'),
  firmServices = require('./api/firm-services'),
  firmUserServices = require('./api/firm-user-services'),
  firmPages = require('./api/firm-pages'),
  firmUserPages = require('./api/firm-user-pages'),
  tours = require('./api/tours'),
  data = require('./api/data'),
  validateFirms = require('./api/validate-firms');

// Check token, expire date, ip
router.all('*', (req, res, next) => {
  const tokenAttr = req.headers.authorization ? req.headers.authorization.split(' ') : null;
  const isOptionRequest = req.headers['access-control-request-method'] ? req.headers['access-control-request-method'] : null;
  const sectionSlug = req.params[0].replace('/', '');

  if (sectionSlug !== 'auth' && sectionSlug !== 'refreshToken' && req.originalMethod !== 'OPTIONS') {
    if (tokenAttr && tokenAttr.length === 2) {
      const token = tokenAttr[1];
      const db = req.app.get('db');

      // Check the existence of the token
      db.Access_tokens.findAll({
        where: {token: token},
        attributes: ['id', 'token', 'ip', 'exp_date', 'user_id']
      })
        .then(data => {
          if (!data.length) {
            res.status(401).send({status: 401, data: []});
          } else {
            tokenData = data[0];

            // Checked tokens expiry date
            if (tokenData.exp_date > moment().unix()) {

              // Check correspondence of IP
              let ip = req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress;

              if (ip === '::1' || ip === '::ffff:127.0.0.1') ip = '127.0.0.1';
              if (tokenData.dataValues.ip === ip) {

                // Updates the token expired date, if all is ok
                db.Access_tokens
                  .update(
                    {exp_date: +moment().add(3, 'hours').unix()},
                    {where: {id: tokenData.dataValues.id}}
                  )
                  .then(data => {
                    next();
                  })
                  .catch(error => {
                    throw new Error(error);
                  });

              } else {
                // Delete token
                console.info('API Delete', tokenData.ip , '===', ip);
                db.Access_tokens
                  .destroy({where: {id: tokenData.dataValues.id}});

                res.status(401).send({status: 401});
              }

            } else {
              res.status(401).send({status: 401});
            }
          }
        })
        .catch(error => {
          console.error(error);
          res.send({status: 500, error: error});
        });
    } else {
      res.status(500).send({status: 500, error: 'TOKEN_ERROR'});
    }
  } else {
    next();
  }
});

// Check user permit
router.all('*', (req, res, next) => {
  const sectionSlug = req.params[0].replace('/', '').split('/')[0];

  if (sectionSlug !== 'auth' && sectionSlug !== 'refreshToken' && req.originalMethod !== 'OPTIONS') {

    // Check existence of the token date and user_id
    if (tokenData && tokenData.user_id) {

      if (req.params && req.params[0]) {

        const db = req.app.get('db'),
          originMethod = req.originalMethod;
        let havePermit = false;

        let query = `SELECT rights.* FROM "Users_rights" as rights, "Rights_blocks" as blocks WHERE user_id = ${tokenData.user_id} AND rights.right_id = blocks.id AND blocks.slug = '${sectionSlug}' LIMIT 1`;
        db.sequelize.query(query)
          .then(rights => {

            if(rights.rowCount >0){
              const userRights = rights[1];

              switch (originMethod) {
                case 'GET' :
                  if (userRights.rows[0].read) havePermit = true;
                  break;
                case 'PUT' :
                  if (userRights.rows[0].update) havePermit = true;
                  break;
                case 'POST' :
                  if (userRights.rows[0].create) havePermit = true;
                  break;
                case 'DELETE' :
                  if (userRights.rows[0].delete) havePermit = true;
                  break;
              }
            } else havePermit = true;

            if (havePermit) {
              next();
            } else {
              res.status(403).send({status: 403, error: 'No have permit'});
            }
          })
          .catch(error => {
            console.error('error', error);
            res.status(200).send({status: 500, error});
          });

      } else {
        next();
      }

    } else {
      res.status(403).send({status: 403});
    }
  } else {
    next();
  }

});

router.get('/', (req, res, next) => {
  res.status(404).json({'error': 'Invalid URL'});
});

router.get('/unauthorized', (req, res, next) => {
  res.status(401).json({'error': 'unauthorized'});
});

module.exports = []
  .concat(router)
  .concat(errors)
  .concat(auth)
  .concat(firms)
  .concat(firmServices)
  .concat(firmUserServices)
  .concat(firmPages)
  .concat(firmUserPages)
  .concat(messages)
  .concat(refreshToken)
  .concat(files)
  .concat(data)
  .concat(tours)
  .concat(validateFirms);