let Sequelize = require('sequelize');
let helper = require('../libs/helper');
let Files = require('../models/Files');


module.exports = (sequelize, DataTypes) => {

  let Firm_user_pages = sequelize.import('../models/Firm_user_pages');
  let Firm_user_services = sequelize.import('../models/Firm_user_services');
  let Firm_moderations = sequelize.import('../models/Firm_moderations');

  let Firms = sequelize.define('Firms', {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      country_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      city_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      image: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true
        }
      },
      phone: {
        type: Sequelize.STRING,
        allowNull: false
      },
      slug: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT
      },
      order: {
        type: Sequelize.INTEGER
      },
      showRecomm: {
        type: Sequelize.BOOLEAN
      },
      errors: {
        type: Sequelize.TEXT
      },
      recomm: {
        type: Sequelize.TEXT
      },
      is_valid: {
        type: Sequelize.BOOLEAN
      },
      status: {
        type: Sequelize.ENUM(1, 2, 3)
      }
    }
  );

  Firms.hasMany(sequelize.models.Firm_user_pages, {
    foreignKey: 'firm_id',
    sourceKey: 'id',
    // constraints: false,
    onDelete: 'CASCADE'
  });
  Firms.hasMany(sequelize.models.Firm_user_services, {
    foreignKey: 'firm_id',
    sourceKey: 'id',
    // constraints: false,
    onDelete: 'CASCADE'
  });
  Firms.hasMany(sequelize.models.Firm_moderations, {
    foreignKey: 'firm_id',
    sourceKey: 'id',
    onDelete: 'CASCADE'
  });

  Firms.beforeValidate((data) => {
    if (!data.slug) {
      data.slug = helper.getSlug(data.name);
    }
  });

  //Firms.beforeBulkDestroy((instance, ) => {
  Firms.beforeBulkDestroy(({where}) => {
    console.log('-----------Firms.beforeBulkDestroy');
    return helper.deleteRelatedFiles(
      sequelize.models,
      sequelize.models.Firms,
      'firms',
      where
    );
  });

  return Firms;
};