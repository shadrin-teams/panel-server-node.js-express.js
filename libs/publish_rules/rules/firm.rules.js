let globalConstants = require('../../../constants');
let constants = require('../constants');

module.exports = {
  list: {
    'common': {
      name: {
        rule: [constants.NOT_NULL],
        require: true
      },
      country_id: {
        rule: [constants.NOT_NULL],
        require: true
      },
      city_id: {
        rule: [constants.NOT_NULL],
        require: true
      }
    },
    'about': {
      image: {
        inTable: {
          table: 'Files',
          where: {section: 'firms', item_id: '#itemId'},
          min: 1
        },
        require: true,
        error: constants.NEED_PUBLISH_LOGO
      },
      description: {
        rule: [constants.NOT_NULL],
        require: true,
        error: constants.NEED_ADD_DESCRIPTION_ABOUT_FIRM
      }
    },
    'services': {
      services: {
        inTable: {
          table: 'Firm_user_services',
          where: {firm_id: '#itemId', is_valid: true},
          min: 1
        },
        require: true,
        error: constants.NEED_ADD_SERVICES
      }
    },
    'pages': {
      contacts: {
        inTable: {
          table: 'Firm_user_pages',
          where: {firm_id: '#itemId', slug: globalConstants.firms.page.CONTACTS, is_valid: true},
          min: 1
        },
        error: constants.NEED_ADD_PAGE
      },
      schedule: {
        inTable: {
          table: 'Firm_user_pages',
          where: {firm_id: '#itemId', slug: globalConstants.firms.page.SCHEDULE, is_valid: true},
          min: 1
        },
        error: constants.NEED_ADD_PAGE
      },
      choose_us: {
        inTable: {
          table: 'Firm_user_pages',
          where: {firm_id: '#itemId', slug: globalConstants.firms.page.CHOOSE_US, is_valid: true},
          min: 1
        },
        error: constants.NEED_ADD_PAGE
      }
    }
  }
};