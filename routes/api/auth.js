let express = require('express'),
  moment = require('moment'),
  _ = require('lodash'),
  queue = require('queue'),
  moduleKey = 'refreshToken',
  router = express.Router(),
  jwt = require('jwt-simple'),
  tokenData,
  db,
  test;

router.get('/auth', (req, res, next) => {
  res.send('+');
});

router.post('/auth', (req, res, next) => {
  const db = req.app.get('db');
  let email = req.body.email || '';
  let password = req.body.password || '';
  let error = '';

  if (email && password) {
    console.log('2', email, password);
    db.Users.findOne({
      where: {email}
    })
      .then((user) => {
        if (!user) {
          throw new Error('Incorrect Email or Password');
        } else {
          let q = queue({concurrency: 1});
          let newAccessToken = '';
          if (db.Users.verifyPassword(user.password, user.salt, password)) {

            let ip = req.connection.remoteAddress && req.connection.remoteAddress !== '::1' ? req.connection.remoteAddress : '127.0.0.1';
            q.push((cb) => {
              db.Access_tokens.findOne({where: {'user_id': user.id}})
                .then((data) => {
                  if (data) {
                    if (ip === data.ip) {
                      data.exp_date = +moment().add(3, 'hours').unix();
                      data.updateAttributes({'exp_date': data.exp_date})
                        .then(() => {
                          newAccessToken = data;
                          cb();
                        })
                        .catch(error => {
                          throw new Error('auth db error');
                        });
                    }
                    else {
                      // Удаляем если сохраненный IP не соответствует IP пользователя
                      db.Access_tokens.destroy({id: data.id})
                        .then(() => {
                          cb();
                        })
                        .catch(err => {
                          throw new Error(err);
                        })
                    }
                  } else {
                    cb();
                  }
                })
                .catch(err => {
                  throw new Error(err);
                });

            });

            // If no exists Toker creating it
            q.push((cb) => {

              if (!error && !newAccessToken) {
                let newAccessTokenData = {
                  'user_id': parseInt(user.id),
                  'exp_date': +moment().add(3, 'hours').unix(),
                  'ip': ip,
                  'token': jwt.encode({id: user.id}, 'devdacticIsAwesome')
                };

                db.Access_tokens.create(newAccessTokenData)
                  .then(newToken => {
                    newAccessToken = newToken;
                    cb();
                  })
                  .catch(err => {
                    throw new Error(err.message);
                  });
              }
              else {
                cb();
              }
            });

            // Get the user list rights
            q.push(cb => {
              if (!error && newAccessToken && user.id) {
                db.Users_rights.findAll({
                  where: {user_id: user.id},
                  attributes: ['right_id', 'create', 'read', 'update_action', 'delete_action']
                })
                  .then(response => {
                    user.rights = [];
                    _.each(response, item => {

                      user.rights.push({
                        right_id: item.dataValues.right_id,
                        create: item.dataValues.create,
                        read: item.dataValues.read,
                        update: item.dataValues.update_action,
                        del: item.dataValues.delete_action
                      });
                    });

                    cb();
                  })
                  .catch(resError => {
                    error = resError;
                    cb();
                  })
              } else {
                cb();
              }
            });

            // Подгружаем название Right_block для правил
            q.push(cb => {
              console.log('6');
              if (!error && newAccessToken && user.rights && user.rights.length) {
                let rightBlock = {};
                db.Rights_blocks.findAll({
                  attributes: ['id', 'slug']
                })
                  .then(listRightBlocks => {

                    _.each(user.rights, (item, index) => {
                      let rightBlock = _.find(listRightBlocks, {id: item.right_id});
                      if (rightBlock) {
                        user.rights[index].block = rightBlock.slug;
                        delete user.rights[index].right_id;
                      }
                    });

                    cb();
                  })
                  .catch(resError => {
                    error = 'listRightBlocks no find';
                    cb();
                  });

              } else {
                cb();
              }
            });

          }
          else throw new Error('incorrect email or password');

          q.start(err => {
            if (!error && !newAccessToken.token) throw new Error('auth token error');
            if (!error && newAccessToken) res.json({
              status: 200,
              'success': 'true',
              'token': newAccessToken.token,
              'token_exp': newAccessToken.exp_date,
              'user': {
                'id': user.id,
                'email': user.email,
                'role': user.role_id,
                first_name: user.first_name,
                last_name: user.last_name,
                avatar: user.avatar,
                sex: user.sex,
                rights: user.rights
              }
            });
            else {
              throw new Error(error);
            }
          });

        }
      })
      .catch((error) => {
        console.log('auth error', error);
        res.status(200).json({status: 500, 'auth': 'fail', error});
      })

  } else {
    error = 'not have required field';
  }
  if (error) res.status(200).json({status: 500, 'auth': 'fail', 'error': error});

});

module.exports = router;