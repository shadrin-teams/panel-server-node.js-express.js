const queue = require('queue');
const fs = require('fs');

class Helper {
  getSlug(name) {
    if(!name)return null;
    const ruList = ['й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'ё', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю'];
    const enList = ['i', 'c', 'u', 'k', 'e', 'n', 'g', 'sh', 'shch', 'z', 'h', '', 'f', 'y', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'zh', 'e', 'e', 'ya', 'ch', 's', 'm', 'i', 't', '', 'b', 'yu'];

    let slug = '';
    let haveMatch = false;
    let slugLoverCase = name.toLowerCase();
    for (let n = 0; n < slugLoverCase.length; n++) {
      haveMatch = false;
      for (let i = 0; i < ruList.length; i++) {
        if (ruList[i] === slugLoverCase[n]) {
          slug += enList[i];
          haveMatch = true;
        }
      }
      if (!haveMatch) slug += slugLoverCase[n];
    }
    slug = slug.replace(/[\.|/\\ ]/g, '-');
    slug = slug.replace(/[^a-z0-9_\-]/gi, '');
    return slug;
  }

  normaliseSequelizeErrors(errors) {
    const arrayOut = {};

    if (errors && Array.isArray(errors)) {
      errors.forEach((item) => {
        let value = item.message;
        if (item.type === 'unique violation' && item.path === 'email') {
          value = 'EMAIL_IS_ALREADY_REGISTERED';
        }
        arrayOut[item.path] = [value];
      });
    }
    return arrayOut;
  }

  deleteRelatedFiles(db, sourceModel, section, where) {
    return new Promise((resolve, reject) => {
      sourceModel.findAll({
        where,
        raw: true
      })
        .then(items => {
          items.forEach(data => {
            db.Files.destroy({
              where: {
                section,
                item_id: data.id
              },
              raw: true
            })
              .then(files => {
                console.warn(`delete files ${section}`, files);
                resolve();
              })
              .catch(error => {
                console.warn('catch', error);
                reject(error);
              })
          });

        })
        .catch(error => {
          reject(error);
        });
      });
  }

  moveFileInFolder(db, oldFolder, newFolderUrl, file) {
    try {
      fs.mkdirSync(`${newFolderUrl}`, error => {
      });
    } catch (error) {
      console.log('error', error);
    }

    try {
      fs.renameSync(`${oldFolder}${file}`, `${newFolderUrl}/${file}`, error => {
        console.log('error2', error);
      });

      fs.renameSync(`${oldFolder}big_${file}`, `${newFolderUrl}/big_${file}`, error => {
        console.log('error3', error);
      });

      fs.renameSync(`${oldFolder}thumb_${file}`, `${newFolderUrl}/thumb_${file}`, error => {
        console.log('error4', error);
      });

    } catch (error) {
      console.log('error', error);
    }

    // rename
  }

  updateOrCreate(model, where, data) {
    return model.findOne({where})
      .then((haveItem) => {
        if (!haveItem) {
          return model.create(data);
        } else {
          return model.update(data, {where});
        }
      });
  }
}

module.exports = new Helper();