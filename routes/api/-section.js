let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  queue = require('queue'),
  moduleKey = 'section',
  sectionModel = require('../../models/Sections'),
  keywordsModel = require('../../models/Keywords'),
  globalError = [];

/**
 * Вызвращает список продукции
 */

router.get('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let id = req.params.id;
  sectionModel.find({site: id,owner:null}, (error, data) => {
    if (error) res.send({status: 'error', error});
    else res.send({status: 'ok', data});
  })
});

router.get('/' + moduleKey + '/:id/:sid', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let id = req.params.id;
  let sid = req.params.sid;

  if (id && sid) {
    console.log('id', sid);
    sectionModel.findOne({id: sid}, function(err, data) {
      if (err) res.send({error: err});
      else res.send({data, status: 'ok'});
    })
  }
  else res.send({data: '', error: 'no category', status: 'fail'});
});

/**
 * Удаление
 */

router.delete('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), (req, res, next) => {

  let id = req.params.id;
  if (id) {

    sectionModel.remove({id: id}, function(err) {
      if (err) res.send({status: false, error: err});
      else {
        // Удалаем все вложенные ключ. слова и вложенные разделы
        let q = queue();
        recursionDelete(q, id);

        q.start(error => {
          console.log(5);
          if (!error) {
            res.send({status: 'deleted'});
          } else {
            res.send({status: false, error: err});
          }
        })
      }
    })
  }
  else res.send({data: '', error: 'no id'});

});

function recursionDelete(q, sectionId) {
  console.log(1);
  q.push(next => {
    console.log(2);
    sectionModel.find({owner: sectionId})
      .then(data => {
        console.log(`Ищем вложенные разделы для #${sectionId}`);
        for (let i = 0; i < data.length; i++) {
          console.log(`Идем удаляеть вложенный раздел #${sectionId}`);
          recursionDelete(q, data[i].id);
        }

        // Удалаяем категорию
        q.push(next => {
          console.log('Удаяем раздел');
          sectionModel.remove({id: sectionId})
            .then(data => {
              console.log(`Ура удалил раздел #${sectionId}`);
              next();
            })
            .catch(error => {
              console.log(`Ошибка удаланеи раздел #${sectionId}`, error);
              globalError.push(error);
              next();
            })
        });

        // Удаляем вложенные ключевые слова
        q.push(next => {
          console.log('Проверяем ключевые слова');
          keywordsModel.find({section: sectionId})
            .then(data => {
              console.log(`Удалем ${data.length} ключей`);
              for (let n = 0; n < data.length; n++) {
                keywordsModel.remove({id: data[n].id})
                  .then(data => {
                    console.log(`Удалем #${data[n].id} ключ`);
                    next();
                  })
                  .catch(error => {
                    console.log(`Ошибка удаления #${data[n].id} ключ`, error, data[n], n);
                    globalError.push(error);
                    next();
                  })
              }
              if (!data.length) {
                next();
              }
            })
            .catch(error => {
              globalError.push(sectionId);
              next();
            })

        });

        next();
      })
      .catch(error => {
        console.log(4);
        globalError.push(error);
        next();
      })
  });
}

router.post('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let data = req.body.data;

  if (data) {

    let model = new sectionModel();
    model.id = model._id;

    Object.keys(data).forEach((field) => {
      model[field] = data[field];
    });
    console.log('add Model', model);
    model.save(function(err, data) {

      if (err) {
        console.log('add err', err, model);
        res.send({error: err});
      }
      else {
        console.log('add ok', model);
        res.send({data: data, status: true});
      }

    });
  } else {
    res.send({data: '', error: 'No data'});
  }

});

router.put('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function(req, res, next) {

  let id = req.params.id,
    data = req.body.data;

  if (data && data.id) {

    sectionModel.findById(data.id, (error, Model) => {
      if (data) {
        Object.keys(data).forEach((field) => {
          Model[field] = data[field];
        });
        Model.save(function(err, data) {
          if (err) {
            res.send({error, status: 'fail'});
          } else {
            res.send({data: {item: data}, status: 'ok'});
          }
        });
      } else {
        res.send({error: 'incorrect id', status: 'fail'});
      }
    });
  } else {
    res.send({'error': 'incorrect data', status: 'fail'});
  }

});

module.exports = router;