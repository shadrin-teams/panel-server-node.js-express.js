let Sequelize = require('sequelize');
let fs = require('fs');

module.exports = (sequelize, DataTypes) => {
  let Files = sequelize.define('Files', {
      file: {
        type: Sequelize.STRING,
        allowNull: false
      },
      folder: {
        type: Sequelize.STRING,
        allowNull: false
      },
      is_used: {
        type: Sequelize.BOOLEAN
      },
      section: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      item_id: {
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      type: {
        type: Sequelize.STRING
      },
      main: {
        type: Sequelize.BOOLEAN
      },
      order: {
        type: Sequelize.INTEGER
      }
    }
  );

  Files.beforeBulkDestroy((instance) => {
    return new Promise((resolve, reject) => {
      sequelize.models.Files.findAll({
        where: instance.where,
        raw: true
      })
        .then(items => {
          items.forEach(item => {
            try {
              fs.unlinkSync(`public/${item.folder}thumb_${item.file}`, (error) => {
              });

              fs.unlinkSync(`public/${item.folder}big_${item.file}`, (error) => {
              });

              fs.unlinkSync(`public/${item.folder}${item.file}`, (error) => {
              });

              fs.rmdirSync(`public/${item.folder}`, (error) => {
              });
            } catch (error) {
            }
          });

          resolve();
        })
        .catch(error => {
          reject(error)
        });

    });
  });

  return Files;
};

