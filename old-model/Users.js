let Sequelize = require('sequelize');

const sequelize = new Sequelize('connectionUri');

//module.exports = sequelize.define('users', {}); // timestamps is false by default

module.exports = sequelize.define('user', {
  email: {
    type: sequelize.STRING
  },
  password: {
    type: sequelize.STRING
  },
  status_id: {
    type: sequelize.INTEGER
  }
});
