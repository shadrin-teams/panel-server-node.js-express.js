let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let Firm_user_services = sequelize.define('Firm_user_services', {
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      firm_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      order: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      is_valid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    }, {
      associate: (models) => {
        Firm_user_services.belongsTo(
          models.Firms, {foreignKey: 'FUS_firm_id', targetKey: 'id', onDelete: 'CASCADE', hooks: true});
      }
    }
  );

  Firm_user_services.beforeBulkDestroy((instance) => {
    console.log('--------Firm_user_services.beforeBulkDestroy');
    return helper.deleteRelatedFiles(
      sequelize.models, sequelize.models.Firm_user_services, 'firms-service', instance.where);
  });

  return Firm_user_services;
};