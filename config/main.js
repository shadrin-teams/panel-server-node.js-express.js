module.exports = {
    'secret': 'devdacticIsAwesome',
    'database': 'mongodb://localhost/keyword_management',
    'baseUrl': 'http://localhost:3002', // 30000
    'publicUrl': 'http://localhost:3002', // 30000
    'publicFolder': 'public/' // 30000
};