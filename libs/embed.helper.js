class EmbedHelper {
  country(db, country_id) {
    return new Promise((resolve) => {
      if (country_id) {
        db.Countries.findById(
          country_id,
          {attributes: ['id', 'name']},
          )
          .then((country) => {
            resolve(country);
          })
          .catch(() => {
            resolve();
          })
      } else {
        resolve();
      }
    });
  }

  city(db, city_id) {
    return new Promise((resolve) => {
      if (city_id) {
        db.Сities.findById(city_id, {attributes: ['id', 'name', 'country_id']})
          .then((city) => {
            resolve(city);
          })
          .catch(() => {
            resolve();
          })
      } else resolve();
    })
  }
}

module.exports = new EmbedHelper();