let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  // let Firms = sequelize.import('../models/Firms');

  let Firm_moderations = sequelize.define('Firm_moderations', {
      firm_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true
      },
      date: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      date_limit: {
        type: Sequelize.DATE,
        allowNull: false
      },
      archive: {
        type: Sequelize.BOOLEAN
      },
      publish_user: {
        type: Sequelize.INTEGER
      },
      publish_date: {
        type: Sequelize.DATE
      }
    }, {
      associate: (models) => {
        models.Firm_moderations.belongsTo(models.Firms)//, {foreignKey: 'f_m_fk_firm_id', targetKey: 'id', onDelete: 'CASCADE', hooks: true});
      }
    }
  );

  return Firm_moderations;
};